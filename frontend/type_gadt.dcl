definition module type_gadt

import syntax
from checksupport import ::Group

create_functions_for_gadt_case_alternatives
	:: !{!Group} !Int !{#CommonDefs} !Int !*{#FunDef} !*ExpressionHeap !*TypeVarHeap !*AttrVarHeap !*VarHeap
							-> (!{!Group},!*{#FunDef},!*ExpressionHeap,!*TypeVarHeap,!*AttrVarHeap,!*VarHeap)
get_var_info_ptr_argument_n :: ![FreeVar] !Int !VarInfoPtr -> Int
get_gadt_case_type_and_vars :: ![AlgebraicPattern] !(Optional GADTResult) !{#CommonDefs} -> Optional GADTResult
get_fun_body_gadt_case_type_and_vars_for_function_with_gadt_case :: !Expression !{#CommonDefs} -> Optional GADTResult
clear_type_variables :: ![TypeVar] !*TypeVarHeap -> *TypeVarHeap
fresh_gadt_type_variables_and_attributes :: !AType ![ATypeVar] ![TypeVar] ![AttributeVar] !*TypeHeaps -> (![TypeVar],!*TypeHeaps)
clear_gadt_type_variables_and_attributes :: !AType ![TypeVar] ![AttributeVar] !*TypeHeaps -> *TypeHeaps
get_more_gadt_cases :: !Int !VarInfoPtr ![FreeVar] !Expression !{#CommonDefs} -> [(VarInfoPtr,GADTResult)]
set_gadt_type_refinements :: !AType !AType !*TypeHeaps -> (!Int,!*TypeHeaps)
refine_function_type :: !SymbolType ![TypeVar] ![[TypeVar]] !*TypeVarHeap !*AttrVarHeap -> (!SymbolType,!*TypeVarHeap,!*AttrVarHeap)
