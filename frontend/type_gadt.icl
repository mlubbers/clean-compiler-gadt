implementation module type_gadt

import StdEnv,StdOverloadedList,compare_types,syntax,utilities
from checksupport import ::Group(..)

:: PatternsGADTTypes
	= GADTTypePattern !(Optional GADTResult)
	| GADTTypePatternWithGADTCase !(Optional GADTResult) ![!PatternsGADTTypes!]
	| GADTTypePatternWithGADTCaseWithDefault !(Optional GADTResult) ![!PatternsGADTTypes!]
	| GADTTypePatternWithGADTCaseWithGADTCaseInDefault !(Optional GADTResult) ![!PatternsGADTTypes!] ![!PatternsGADTTypes!]
	| KeepGADTPattern

create_functions_for_gadt_case_alternatives
	:: !{!Group} !Int !{#CommonDefs} !Int !*{#FunDef} !*ExpressionHeap !*TypeVarHeap !*AttrVarHeap !*VarHeap
							-> (!{!Group},!*{#FunDef},!*ExpressionHeap,!*TypeVarHeap,!*AttrVarHeap,!*VarHeap)
create_functions_for_gadt_case_alternatives groups next_group_number common_defs main_module_n fun_defs expr_heap type_var_heap attr_var_heap var_heap
	# (next_fun_def_i,fun_defs) = usize fun_defs
	# (rev_new_fun_defs,next_fun_def_i,fun_defs,expr_heap,type_var_heap,attr_var_heap,var_heap)
		= create_functions_for_gadt_case_alternatives 0 [!!] next_fun_def_i next_group_number groups common_defs fun_defs
			expr_heap type_var_heap attr_var_heap var_heap
	| rev_new_fun_defs=:[!!]
		= (groups,fun_defs,expr_heap,type_var_heap,attr_var_heap,var_heap)
		# (first_new_function_n,fun_defs) = usize fun_defs
		  fun_defs = arrayPlusRevStrictList fun_defs rev_new_fun_defs
		  (inc_last_new_function_n,fun_defs) = usize fun_defs;
		  groups = arrayPlusStrictList groups [!{group_members=[function_n]} \\ function_n<-[first_new_function_n..inc_last_new_function_n-1]!]
		= (groups,fun_defs,expr_heap,type_var_heap,attr_var_heap,var_heap)
where
	create_functions_for_gadt_case_alternatives
		:: !Int ![!FunDef!] !Int !Int !{!Group} !{#CommonDefs} !*{#FunDef} !*ExpressionHeap !*TypeVarHeap !*AttrVarHeap !*VarHeap
										  -> (![!FunDef!],!Int,!*{#FunDef},!*ExpressionHeap,!*TypeVarHeap,!*AttrVarHeap,!*VarHeap)
	create_functions_for_gadt_case_alternatives group_i rev_new_fun_defs next_fun_def_i next_group_number groups common_defs fun_defs 
			expr_heap type_var_heap attr_var_heap var_heap
		| group_i<size groups
			# (rev_new_fun_defs,next_fun_def_i,next_group_number,fun_defs,expr_heap,type_var_heap,attr_var_heap,var_heap)
				= create_functions_for_gadt_case_alternatives_of_group groups.[group_i].group_members rev_new_fun_defs next_fun_def_i next_group_number
					common_defs fun_defs expr_heap type_var_heap attr_var_heap var_heap
			= create_functions_for_gadt_case_alternatives (group_i+1) rev_new_fun_defs next_fun_def_i next_group_number groups common_defs fun_defs
				expr_heap type_var_heap attr_var_heap var_heap
			= (rev_new_fun_defs,next_fun_def_i,fun_defs,expr_heap,type_var_heap,attr_var_heap,var_heap)

	create_functions_for_gadt_case_alternatives_of_group
		:: ![Int] ![!FunDef!] !Int !Int !{#CommonDefs} !*{#FunDef} !*ExpressionHeap !*TypeVarHeap !*AttrVarHeap !*VarHeap
							 -> (![!FunDef!],!Int,!Int,!*{#FunDef},!*ExpressionHeap,!*TypeVarHeap,!*AttrVarHeap,!*VarHeap)
	create_functions_for_gadt_case_alternatives_of_group [fun_i:fun_is] rev_new_fun_defs next_fun_def_i next_group_number common_defs
			fun_defs expr_heap type_var_heap attr_var_heap var_heap
		# (fun_def=:{fun_type,fun_body,fun_info},fun_defs) = fun_defs![fun_i]
		| fun_type =: FunDefType _
			= case fun_body of
				TransformedBody {tb_args,tb_rhs=Case kase=:{case_expr=Var {var_info_ptr},case_guards=AlgebraicPatterns gi=:{gi_module,gi_index} patterns,case_default,case_explicit=False}}
					| not common_defs.[gi_module].com_type_defs.[gi_index].td_rhs =: GeneralisedAlgType _
						-> create_functions_for_gadt_case_alternatives_of_group fun_is rev_new_fun_defs next_fun_def_i next_group_number common_defs
							fun_defs expr_heap type_var_heap attr_var_heap var_heap
					-> case case_default of
						No
							| not (should_transform_gadt_function patterns tb_args [var_info_ptr] common_defs)
								-> create_functions_for_gadt_case_alternatives_of_group fun_is rev_new_fun_defs next_fun_def_i next_group_number common_defs fun_defs
									expr_heap type_var_heap attr_var_heap var_heap

								# gadt_types = gadt_types_of_patterns patterns tb_args [var_info_ptr] common_defs

								# (fun_body,local_vars,fun_calls,dynamics,rev_new_fun_defs,next_fun_def_i,next_group_number,expr_heap,type_var_heap,attr_var_heap,var_heap)
									= create_functions_for_gadt_case_alternatives_of_function_and_update_case patterns gadt_types
										fun_def rev_new_fun_defs next_fun_def_i next_group_number
										common_defs main_module_n expr_heap type_var_heap attr_var_heap var_heap

								# fun_info & fi_calls=fun_calls, fi_local_vars=local_vars, fi_dynamics=dynamics
								# fun_defs & [fun_i].fun_body = fun_body, [fun_i].fun_info=fun_info
								-> create_functions_for_gadt_case_alternatives_of_group fun_is rev_new_fun_defs next_fun_def_i next_group_number common_defs fun_defs
									expr_heap type_var_heap attr_var_heap var_heap
						Yes default_expr
							| not (should_transform_gadt_function_with_default patterns tb_args [var_info_ptr] common_defs)
								-> create_functions_for_gadt_case_alternatives_of_group fun_is rev_new_fun_defs next_fun_def_i next_group_number common_defs
									fun_defs expr_heap type_var_heap attr_var_heap var_heap

								# gadt_types = gadt_types_of_patterns_with_default patterns tb_args [var_info_ptr] common_defs

								# (fun_body,local_vars,fun_calls,dynamics,rev_new_fun_defs,next_fun_def_i,next_group_number,expr_heap,type_var_heap,attr_var_heap,var_heap)
									= create_functions_for_gadt_case_alternatives_of_function_and_update_case patterns gadt_types
										fun_def rev_new_fun_defs next_fun_def_i next_group_number
										common_defs main_module_n expr_heap type_var_heap attr_var_heap var_heap

								# fun_info & fi_calls=fun_calls, fi_local_vars=local_vars, fi_dynamics=dynamics
								# fun_defs & [fun_i].fun_body = fun_body, [fun_i].fun_info=fun_info
								-> create_functions_for_gadt_case_alternatives_of_group fun_is rev_new_fun_defs next_fun_def_i next_group_number common_defs
									fun_defs expr_heap type_var_heap attr_var_heap var_heap
				_
					-> create_functions_for_gadt_case_alternatives_of_group fun_is rev_new_fun_defs next_fun_def_i next_group_number common_defs
						fun_defs expr_heap type_var_heap attr_var_heap var_heap
			= create_functions_for_gadt_case_alternatives_of_group fun_is rev_new_fun_defs next_fun_def_i next_group_number common_defs
				fun_defs expr_heap type_var_heap attr_var_heap var_heap
	where
		should_transform_gadt_function :: ![AlgebraicPattern] ![FreeVar] ![VarInfoPtr] !{#CommonDefs} -> Bool
		should_transform_gadt_function [pattern=:{
				ap_expr=Case {case_expr=Var {var_info_ptr},
							  case_guards=AlgebraicPatterns gi=:{gi_module,gi_index} case_patterns,case_default,
							  case_explicit=False},
				ap_symbol={glob_object,glob_module}}:patterns] args previous_case_vars common_defs
			| common_defs.[gi_module].com_type_defs.[gi_index].td_rhs =: GeneralisedAlgType _
			&& is_arg var_info_ptr args && not (is_previous_case_var var_info_ptr previous_case_vars)
				# cons_gadt_type = common_defs.[glob_module].com_cons_defs.[glob_object.ds_index].cons_gadt_type
				# should_transform_gadt_function_in_patterns = should_transform_gadt_function patterns args previous_case_vars common_defs
				# previous_case_vars = [var_info_ptr:previous_case_vars]
				= case case_default of
					No
						# should_transform_gadt_function_in_case = should_transform_gadt_function case_patterns args previous_case_vars common_defs
						-> should_transform_gadt_function_in_case || should_transform_gadt_function_in_patterns
					Yes (Case {case_expr=Var {var_info_ptr=default_case_var_info_ptr},case_guards=AlgebraicPatterns gi=:{gi_module,gi_index} default_patterns,case_default,case_explicit=False})
						| common_defs.[gi_module].com_type_defs.[gi_index].td_rhs =: GeneralisedAlgType _
						&& is_arg default_case_var_info_ptr args && not (is_previous_case_var default_case_var_info_ptr previous_case_vars)
							# should_transform_gadt_function_in_case = should_transform_gadt_function_with_default case_patterns args previous_case_vars common_defs
							# previous_case_vars = [default_case_var_info_ptr:previous_case_vars]
							# gadt_types_default = should_transform_gadt_function default_patterns args previous_case_vars common_defs
							-> should_transform_gadt_function_in_case || gadt_types_default || should_transform_gadt_function_in_patterns

							# should_transform_gadt_function_in_case = should_transform_gadt_function_with_default case_patterns args previous_case_vars common_defs
							-> cons_gadt_type=:Yes _ || should_transform_gadt_function_in_case || should_transform_gadt_function_in_patterns
					Yes _
						# should_transform_gadt_function_in_case = should_transform_gadt_function_with_default case_patterns args previous_case_vars common_defs
						-> cons_gadt_type=:Yes _ || should_transform_gadt_function_in_case || should_transform_gadt_function_in_patterns
		should_transform_gadt_function [pattern=:{ap_symbol={glob_object,glob_module},ap_expr}:patterns] args previous_case_vars common_defs
			# cons_gadt_type = common_defs.[glob_module].com_cons_defs.[glob_object.ds_index].cons_gadt_type
			# should_transform_gadt_function_in_patterns = should_transform_gadt_function patterns args previous_case_vars common_defs
			= cons_gadt_type=:Yes _ || should_transform_gadt_function_in_patterns
		should_transform_gadt_function [] args previous_case_vars common_defs
			= False

		should_transform_gadt_function_with_default :: ![AlgebraicPattern] ![FreeVar] ![VarInfoPtr] !{#CommonDefs} -> Bool
		should_transform_gadt_function_with_default [{
				ap_expr=ap_expr=:Case {case_expr=Var {var_info_ptr},
									   case_guards=AlgebraicPatterns gi=:{gi_module,gi_index} case_patterns,
									   case_explicit=False,case_default},
				ap_symbol={glob_object,glob_module}}:patterns] args previous_case_vars common_defs
			| common_defs.[gi_module].com_type_defs.[gi_index].td_rhs =: GeneralisedAlgType _
			&& is_arg var_info_ptr args && not (is_previous_case_var var_info_ptr previous_case_vars)
				# should_transform_gadt_function_in_patterns = should_transform_gadt_function_with_default patterns args previous_case_vars common_defs
				# previous_case_vars = [var_info_ptr:previous_case_vars]
				= case case_default of
					No
						# should_transform_gadt_function_in_case = should_transform_gadt_function_with_default case_patterns args previous_case_vars common_defs
						-> should_transform_gadt_function_in_case || should_transform_gadt_function_in_patterns
					Yes default_expr=:(Case {case_expr=Var {var_info_ptr=default_case_var_info_ptr},case_guards=AlgebraicPatterns gi=:{gi_module,gi_index} default_patterns,case_default,case_explicit=False})
						| common_defs.[gi_module].com_type_defs.[gi_index].td_rhs =: GeneralisedAlgType _
						&& is_arg default_case_var_info_ptr args && not (is_previous_case_var default_case_var_info_ptr previous_case_vars)
							# cons_gadt_type = common_defs.[glob_module].com_cons_defs.[glob_object.ds_index].cons_gadt_type
							# should_transform_gadt_function_in_case = should_transform_gadt_function_with_default case_patterns args previous_case_vars common_defs
							# previous_case_vars = [default_case_var_info_ptr:previous_case_vars]
							# gadt_types_default = should_transform_gadt_function_with_default default_patterns args previous_case_vars common_defs
							-> should_transform_gadt_function_in_case || gadt_types_default || should_transform_gadt_function_in_patterns
							| alternative_may_fail default_expr common_defs
								| alternative_may_fail ap_expr common_defs
									-> should_transform_gadt_function_in_patterns
									# cons_gadt_type = common_defs.[glob_module].com_cons_defs.[glob_object.ds_index].cons_gadt_type
									-> cons_gadt_type=:Yes _ || should_transform_gadt_function_in_patterns
								# cons_gadt_type = common_defs.[glob_module].com_cons_defs.[glob_object.ds_index].cons_gadt_type
								# should_transform_gadt_function_in_case = should_transform_gadt_function_with_default case_patterns args previous_case_vars common_defs
								-> cons_gadt_type=:Yes _ || should_transform_gadt_function_in_case || should_transform_gadt_function_in_patterns
					Yes default_expr
						| alternative_may_fail default_expr common_defs
							| alternative_may_fail ap_expr common_defs
								-> should_transform_gadt_function_in_patterns
								# cons_gadt_type = common_defs.[glob_module].com_cons_defs.[glob_object.ds_index].cons_gadt_type
								-> cons_gadt_type=:Yes _ || should_transform_gadt_function_in_patterns
							# cons_gadt_type = common_defs.[glob_module].com_cons_defs.[glob_object.ds_index].cons_gadt_type
							# should_transform_gadt_function_in_case = should_transform_gadt_function_with_default case_patterns args previous_case_vars common_defs
							-> cons_gadt_type=:Yes _ || should_transform_gadt_function_in_case || should_transform_gadt_function_in_patterns
		should_transform_gadt_function_with_default [{ap_symbol={glob_object,glob_module},ap_expr}:patterns] args previous_case_vars common_defs
			# cons_gadt_type = common_defs.[glob_module].com_cons_defs.[glob_object.ds_index].cons_gadt_type
			# should_transform_gadt_function_in_patterns = should_transform_gadt_function_with_default patterns args previous_case_vars common_defs
			| alternative_may_fail ap_expr common_defs
				= should_transform_gadt_function_in_patterns
				# cons_gadt_type = common_defs.[glob_module].com_cons_defs.[glob_object.ds_index].cons_gadt_type
				= cons_gadt_type=:Yes _ || should_transform_gadt_function_in_patterns
		should_transform_gadt_function_with_default [] args previous_case_vars common_defs
			= False

		gadt_types_of_patterns :: ![AlgebraicPattern] ![FreeVar] ![VarInfoPtr] {#CommonDefs} -> [!PatternsGADTTypes!]
		gadt_types_of_patterns [pattern=:{
				ap_expr=Case {case_expr=Var {var_info_ptr},
							  case_guards=AlgebraicPatterns gi=:{gi_module,gi_index} case_patterns,case_default,
							  case_explicit=False},
				ap_symbol={glob_object,glob_module}}:patterns] args previous_case_vars common_defs
			#! cons_gadt_type = common_defs.[glob_module].com_cons_defs.[glob_object.ds_index].cons_gadt_type
			# gadt_types = gadt_types_of_patterns patterns args previous_case_vars common_defs
			| common_defs.[gi_module].com_type_defs.[gi_index].td_rhs =: GeneralisedAlgType _
			&& is_arg var_info_ptr args && not (is_previous_case_var var_info_ptr previous_case_vars)
				# previous_case_vars = [var_info_ptr:previous_case_vars]
				= case case_default of
					No
						# case_gadt_types = gadt_types_of_patterns case_patterns args previous_case_vars common_defs
						-> [!GADTTypePatternWithGADTCase cons_gadt_type case_gadt_types:gadt_types!]
					Yes (Case {case_expr=Var {var_info_ptr=default_case_var_info_ptr},case_guards=AlgebraicPatterns gi=:{gi_module,gi_index} default_patterns,case_default,case_explicit=False})
						| common_defs.[gi_module].com_type_defs.[gi_index].td_rhs =: GeneralisedAlgType _
						&& is_arg default_case_var_info_ptr args && not (is_previous_case_var default_case_var_info_ptr previous_case_vars)
							# case_gadt_types = gadt_types_of_patterns_with_default case_patterns args previous_case_vars common_defs
							# previous_case_vars = [default_case_var_info_ptr:previous_case_vars]
							# gadt_types_default = gadt_types_of_patterns default_patterns args previous_case_vars common_defs
							-> [!GADTTypePatternWithGADTCaseWithGADTCaseInDefault cons_gadt_type case_gadt_types gadt_types_default
							    :gadt_types!]
							# case_gadt_types = gadt_types_of_patterns_with_default case_patterns args previous_case_vars common_defs
							-> [!GADTTypePatternWithGADTCaseWithDefault cons_gadt_type case_gadt_types:gadt_types!]
					Yes _
						# case_gadt_types = gadt_types_of_patterns_with_default case_patterns args previous_case_vars common_defs
						-> [!GADTTypePatternWithGADTCaseWithDefault cons_gadt_type case_gadt_types:gadt_types!]
		gadt_types_of_patterns [pattern=:{ap_symbol={glob_object,glob_module},ap_expr}:patterns] args previous_case_vars common_defs
			# cons_gadt_type = common_defs.[glob_module].com_cons_defs.[glob_object.ds_index].cons_gadt_type
			# gadt_types = gadt_types_of_patterns patterns args previous_case_vars common_defs
			= [!GADTTypePattern cons_gadt_type:gadt_types!]
		gadt_types_of_patterns [] args previous_case_vars common_defs
			= [!!]

		gadt_types_of_patterns_with_default :: ![AlgebraicPattern] ![FreeVar] ![VarInfoPtr] {#CommonDefs} -> [!PatternsGADTTypes!]
		gadt_types_of_patterns_with_default [pattern=:{
				ap_expr=ap_expr=:Case {case_expr=Var {var_info_ptr},
									   case_guards=AlgebraicPatterns gi=:{gi_module,gi_index} case_patterns,
									   case_explicit=False,case_default},
				ap_symbol={glob_object,glob_module}}:patterns] args previous_case_vars common_defs
			# gadt_types = gadt_types_of_patterns_with_default patterns args previous_case_vars common_defs
			| common_defs.[gi_module].com_type_defs.[gi_index].td_rhs =: GeneralisedAlgType _
			&& is_arg var_info_ptr args && not (is_previous_case_var var_info_ptr previous_case_vars)
				# previous_case_vars = [var_info_ptr:previous_case_vars]
				= case case_default of
					No
						#! cons_gadt_type = common_defs.[glob_module].com_cons_defs.[glob_object.ds_index].cons_gadt_type
						# case_gadt_types = gadt_types_of_patterns_with_default case_patterns args previous_case_vars common_defs
						-> [!GADTTypePatternWithGADTCase cons_gadt_type case_gadt_types:gadt_types!]

					Yes default_expr=:(Case {case_expr=Var {var_info_ptr=default_case_var_info_ptr},case_guards=AlgebraicPatterns gi=:{gi_module,gi_index} default_patterns,case_default,case_explicit=False})
						| common_defs.[gi_module].com_type_defs.[gi_index].td_rhs =: GeneralisedAlgType _
						&& is_arg default_case_var_info_ptr args && not (is_previous_case_var default_case_var_info_ptr previous_case_vars)
							# cons_gadt_type = common_defs.[glob_module].com_cons_defs.[glob_object.ds_index].cons_gadt_type
							# case_gadt_types = gadt_types_of_patterns_with_default case_patterns args previous_case_vars common_defs
							# previous_case_vars = [default_case_var_info_ptr:previous_case_vars]
							# gadt_types_default = gadt_types_of_patterns_with_default default_patterns args previous_case_vars common_defs
							-> [!GADTTypePatternWithGADTCaseWithGADTCaseInDefault cons_gadt_type case_gadt_types gadt_types_default
							    :gadt_types!]

							| alternative_may_fail default_expr common_defs
								| alternative_may_fail ap_expr common_defs
									-> [!KeepGADTPattern:gadt_types!]
									#! cons_gadt_type = common_defs.[glob_module].com_cons_defs.[glob_object.ds_index].cons_gadt_type
									-> [!GADTTypePattern cons_gadt_type:gadt_types!]
								#! cons_gadt_type = common_defs.[glob_module].com_cons_defs.[glob_object.ds_index].cons_gadt_type
								# case_gadt_types = gadt_types_of_patterns_with_default case_patterns args previous_case_vars common_defs
								-> [!GADTTypePatternWithGADTCaseWithDefault cons_gadt_type case_gadt_types:gadt_types!]

					Yes default_expr
						| alternative_may_fail default_expr common_defs
							| alternative_may_fail ap_expr common_defs
								-> [!KeepGADTPattern:gadt_types!]
								#! cons_gadt_type = common_defs.[glob_module].com_cons_defs.[glob_object.ds_index].cons_gadt_type
								-> [!GADTTypePattern cons_gadt_type:gadt_types!]
							#! cons_gadt_type = common_defs.[glob_module].com_cons_defs.[glob_object.ds_index].cons_gadt_type
							# case_gadt_types = gadt_types_of_patterns_with_default case_patterns args previous_case_vars common_defs
							-> [!GADTTypePatternWithGADTCaseWithDefault cons_gadt_type case_gadt_types:gadt_types!]
		gadt_types_of_patterns_with_default [pattern=:{ap_symbol={glob_object,glob_module},ap_expr}:patterns] args previous_case_vars common_defs
			# gadt_types = gadt_types_of_patterns_with_default patterns args previous_case_vars common_defs
			| alternative_may_fail ap_expr common_defs
				= [!KeepGADTPattern:gadt_types!]
				#! cons_gadt_type = common_defs.[glob_module].com_cons_defs.[glob_object.ds_index].cons_gadt_type
				= [!GADTTypePattern cons_gadt_type:gadt_types!]
		gadt_types_of_patterns_with_default [] args previous_case_vars common_defs
			= [!!]

		is_arg var_info_ptr [{fv_info_ptr}:args] = var_info_ptr==fv_info_ptr || is_arg var_info_ptr args
		is_arg var_info_ptr [] = False

		is_previous_case_var var_info_ptr [fv_info_ptr:args] = var_info_ptr==fv_info_ptr || is_previous_case_var var_info_ptr args
		is_previous_case_var var_info_ptr [] = False

		alternative_may_fail (Case {case_explicit,case_guards,case_default}) common_defs
			| case_explicit
				= False
			= case case_default of
				Yes default_expr
					-> alternative_may_fail default_expr common_defs
				No
					| all_constructors_matched case_guards common_defs
						-> an_alternative_may_fail case_guards common_defs
						-> True
		alternative_may_fail (Let {let_expr}) common_defs
			= alternative_may_fail let_expr common_defs
		alternative_may_fail _ common_defs
			= False

		an_alternative_may_fail (AlgebraicPatterns _ algebraic_patterns) common_defs
			= an_algebraic_patterns_may_fail algebraic_patterns common_defs
		an_alternative_may_fail (OverloadedPatterns _ _ algebraic_patterns) common_defs
			= an_algebraic_patterns_may_fail algebraic_patterns common_defs
		an_alternative_may_fail _ common_defs
			= True

		an_algebraic_patterns_may_fail [{ap_expr}:algebraic_patterns] common_defs
			| alternative_may_fail ap_expr common_defs
				= True
				= an_algebraic_patterns_may_fail algebraic_patterns common_defs
		an_algebraic_patterns_may_fail [] common_defs
			= False

		// also in module convertcases
		all_constructors_matched (AlgebraicPatterns global_type_index patterns) common_defs
			= case common_defs.[global_type_index.gi_module].com_type_defs.[global_type_index.gi_index].td_rhs of
				AlgType cons_symbols
					-> same_length cons_symbols patterns
				GeneralisedAlgType cons_symbols
					-> same_length cons_symbols patterns
				_
					-> False
		where
			same_length [_:l1] [_:l2] = same_length l1 l2
			same_length [] [] = True
			same_length _ _ = False
		all_constructors_matched (OverloadedPatterns _ _ [_,_]) common_defs
			= True
		all_constructors_matched case_guards common_defs
			= False

		create_functions_for_gadt_case_alternatives_of_function_and_update_case
			:: ![AlgebraicPattern] ![!PatternsGADTTypes!] FunDef [!FunDef!] Int Int {#CommonDefs} Int
																						 !*ExpressionHeap !*TypeVarHeap !*AttrVarHeap !*VarHeap
			-> (!FunctionBody,![FreeVar],![FunCall],![ExprInfoPtr],![!FunDef!],!Int,!Int,!*ExpressionHeap,!*TypeVarHeap,!*AttrVarHeap,!*VarHeap)

		create_functions_for_gadt_case_alternatives_of_function_and_update_case patterns gadt_types
				fun_def=:{fun_body=TransformedBody {tb_args,tb_rhs=Case kase=:{case_guards=AlgebraicPatterns gi _}}} rev_new_fun_defs
				next_fun_def_i next_group_number common_defs main_module_n expr_heap type_var_heap attr_var_heap var_heap
			# (patterns,rev_new_fun_defs,next_fun_def_i,next_group_number,expr_heap,type_var_heap,attr_var_heap,var_heap)
				= create_functions_for_gadt_case_alternatives_of_function gadt_types patterns
					fun_def rev_new_fun_defs next_fun_def_i next_group_number common_defs main_module_n expr_heap type_var_heap attr_var_heap var_heap
			# rhs = Case {kase & case_guards = AlgebraicPatterns gi patterns}

			# (fun_body,local_vars,fun_calls,dynamics,expr_heap,var_heap)
				= replace_variables_determine_ref_counts_and_calls tb_args rhs expr_heap var_heap
			= (fun_body,local_vars,fun_calls,dynamics,rev_new_fun_defs,next_fun_def_i,next_group_number,expr_heap,type_var_heap,attr_var_heap,var_heap)
	create_functions_for_gadt_case_alternatives_of_group [] rev_new_fun_defs next_fun_def_i next_group_number common_defs
			fun_defs expr_heap type_var_heap attr_var_heap var_heap
		= (rev_new_fun_defs,next_fun_def_i,next_group_number,fun_defs,expr_heap,type_var_heap,attr_var_heap,var_heap)

:: CopyState = {
	cs_type_var_heap :: !.TypeVarHeap,
	cs_attr_var_heap :: !.AttrVarHeap,
	cs_var_heap :: !.VarHeap
   }

:: CasesAndPatterns
	= RootCase !Expression
	| RootCaseAndDefault !Expression !Expression
	| RootCaseAndPattern !Expression !AlgebraicPattern
	| RootCasePatternAndDefault !Expression !AlgebraicPattern !Expression
	| CasesAndPatterns !CasesAndPatterns !AlgebraicPattern
	| CasesAndPatternsAndDefault !CasesAndPatterns !AlgebraicPattern !Expression

create_functions_for_gadt_case_alternatives_of_function
	:: ![!PatternsGADTTypes!] ![AlgebraicPattern] !FunDef ![!FunDef!] !Int !Int !{#CommonDefs} !Int
												  !*ExpressionHeap !*TypeVarHeap !*AttrVarHeap !*VarHeap
	-> (![AlgebraicPattern],![!FunDef!],!Int,!Int,!*ExpressionHeap,!*TypeVarHeap,!*AttrVarHeap,!*VarHeap)
create_functions_for_gadt_case_alternatives_of_function
		[!gadt_type_pattern=:GADTTypePattern (Yes gadt_type):gadt_types!]
		[pattern:patterns]
		fun_def=:{fun_body = TransformedBody {tb_args,tb_rhs}}
		rev_new_fun_defs next_fun_def_i next_group_number common_defs main_module_n expr_heap type_var_heap attr_var_heap var_heap
	# (pattern,rev_new_fun_defs,next_fun_def_i,next_group_number,expr_heap,type_var_heap,attr_var_heap,var_heap)
		= create_new_gadt_type_function pattern (RootCase tb_rhs) tb_args fun_def
			rev_new_fun_defs next_fun_def_i next_group_number common_defs main_module_n expr_heap type_var_heap attr_var_heap var_heap
	# (patterns,rev_new_fun_defs,next_fun_def_i,next_group_number,expr_heap,type_var_heap,attr_var_heap,var_heap)
		= create_functions_for_gadt_case_alternatives_of_function gadt_types patterns
			fun_def rev_new_fun_defs next_fun_def_i next_group_number common_defs main_module_n expr_heap type_var_heap attr_var_heap var_heap
	= ([pattern:patterns],rev_new_fun_defs,next_fun_def_i,next_group_number,expr_heap,type_var_heap,attr_var_heap,var_heap)
create_functions_for_gadt_case_alternatives_of_function
		[!gadt_type_pattern=:GADTTypePattern No:gadt_types!] [pattern:patterns] fun_def
		rev_new_fun_defs next_fun_def_i next_group_number common_defs main_module_n expr_heap type_var_heap attr_var_heap var_heap
	# (patterns,rev_new_fun_defs,next_fun_def_i,next_group_number,expr_heap,type_var_heap,attr_var_heap,var_heap)
		= create_functions_for_gadt_case_alternatives_of_function gadt_types patterns
			fun_def rev_new_fun_defs next_fun_def_i next_group_number common_defs main_module_n expr_heap type_var_heap attr_var_heap var_heap
	= ([pattern:patterns],rev_new_fun_defs,next_fun_def_i,next_group_number,expr_heap,type_var_heap,attr_var_heap,var_heap)
create_functions_for_gadt_case_alternatives_of_function
		[!gadt_type_pattern=:GADTTypePatternWithGADTCase (Yes _) gadt_types_case_patterns:gadt_types!]
		[pattern=:{ap_vars,ap_expr=Case kees=:{case_guards=AlgebraicPatterns pattern_gi case_patterns}}:patterns]
		fun_def=:{fun_body = TransformedBody {tb_args,tb_rhs}}
		rev_new_fun_defs next_fun_def_i next_group_number common_defs main_module_n expr_heap type_var_heap attr_var_heap var_heap
	# (updated_case_patterns,rev_new_fun_defs,next_fun_def_i,next_group_number,expr_heap,type_var_heap,attr_var_heap,var_heap)
		= create_new_gadt_type_functions gadt_types_case_patterns case_patterns (RootCaseAndPattern tb_rhs pattern) tb_args fun_def
			rev_new_fun_defs next_fun_def_i next_group_number common_defs main_module_n expr_heap type_var_heap attr_var_heap var_heap
	# (new_ap_vars,var_heap) = create_new_args_and_forward_old_args pattern.ap_vars var_heap
	  pattern & ap_expr=Case {kees & case_guards=AlgebraicPatterns pattern_gi updated_case_patterns}, ap_vars=new_ap_vars
	# (patterns,rev_new_fun_defs,next_fun_def_i,next_group_number,expr_heap,type_var_heap,attr_var_heap,var_heap)
		= create_functions_for_gadt_case_alternatives_of_function gadt_types patterns
			fun_def rev_new_fun_defs next_fun_def_i next_group_number common_defs main_module_n expr_heap type_var_heap attr_var_heap var_heap
	= ([pattern:patterns],rev_new_fun_defs,next_fun_def_i,next_group_number,expr_heap,type_var_heap,attr_var_heap,var_heap)
create_functions_for_gadt_case_alternatives_of_function
		[!gadt_type_pattern=:GADTTypePatternWithGADTCase No gadt_types_case_patterns:gadt_types!]
		[pattern=:{ap_vars,ap_expr=Case kees=:{case_guards=AlgebraicPatterns pattern_gi case_patterns}}:patterns]
		fun_def=:{fun_body = TransformedBody {tb_args,tb_rhs}}
		rev_new_fun_defs next_fun_def_i next_group_number common_defs main_module_n expr_heap type_var_heap attr_var_heap var_heap
	# (updated_case_patterns,rev_new_fun_defs,next_fun_def_i,next_group_number,expr_heap,type_var_heap,attr_var_heap,var_heap)
		= create_new_gadt_type_functions_if_gadt_type gadt_types_case_patterns case_patterns (RootCaseAndPattern tb_rhs pattern) tb_args fun_def
			rev_new_fun_defs next_fun_def_i next_group_number common_defs main_module_n expr_heap type_var_heap attr_var_heap var_heap
	# (new_ap_vars,var_heap) = create_new_args_and_forward_old_args pattern.ap_vars var_heap
	  pattern & ap_expr=Case {kees & case_guards=AlgebraicPatterns pattern_gi updated_case_patterns}, ap_vars=new_ap_vars
	# (patterns,rev_new_fun_defs,next_fun_def_i,next_group_number,expr_heap,type_var_heap,attr_var_heap,var_heap)
		= create_functions_for_gadt_case_alternatives_of_function gadt_types patterns
			fun_def rev_new_fun_defs next_fun_def_i next_group_number common_defs main_module_n expr_heap type_var_heap attr_var_heap var_heap
	= ([pattern:patterns],rev_new_fun_defs,next_fun_def_i,next_group_number,expr_heap,type_var_heap,attr_var_heap,var_heap)
create_functions_for_gadt_case_alternatives_of_function
		[!gadt_type_pattern=:GADTTypePatternWithGADTCaseWithDefault (Yes _) gadt_types_case_patterns:gadt_types!]
		[pattern=:{ap_expr=Case {case_guards=AlgebraicPatterns _ case_patterns,case_default=Yes default_expr}}:patterns]
		fun_def=:{fun_body = TransformedBody {tb_args,tb_rhs=tb_rhs}}
		rev_new_fun_defs next_fun_def_i next_group_number common_defs main_module_n expr_heap type_var_heap attr_var_heap var_heap
	# (updated_case_patterns,rev_new_fun_defs,next_fun_def_i,next_group_number,expr_heap,type_var_heap,attr_var_heap,var_heap)
		= create_new_gadt_type_functions gadt_types_case_patterns case_patterns (RootCaseAndPattern tb_rhs pattern) tb_args fun_def
			rev_new_fun_defs next_fun_def_i next_group_number common_defs main_module_n expr_heap type_var_heap attr_var_heap var_heap
	# (pattern,rev_new_fun_defs,next_fun_def_i,next_group_number,expr_heap,type_var_heap,attr_var_heap,var_heap)
		= create_new_gadt_type_function_for_default_and_updated_case_patterns updated_case_patterns pattern
			(RootCaseAndDefault tb_rhs default_expr) tb_args fun_def
			rev_new_fun_defs next_fun_def_i next_group_number common_defs main_module_n expr_heap type_var_heap attr_var_heap var_heap
	# (patterns,rev_new_fun_defs,next_fun_def_i,next_group_number,expr_heap,type_var_heap,attr_var_heap,var_heap)
		= create_functions_for_gadt_case_alternatives_of_function gadt_types patterns
			fun_def rev_new_fun_defs next_fun_def_i next_group_number common_defs main_module_n expr_heap type_var_heap attr_var_heap var_heap
	= ([pattern:patterns],rev_new_fun_defs,next_fun_def_i,next_group_number,expr_heap,type_var_heap,attr_var_heap,var_heap)
create_functions_for_gadt_case_alternatives_of_function
		[!gadt_type_pattern=:GADTTypePatternWithGADTCaseWithDefault No gadt_types_case_patterns:gadt_types!]
		[pattern=:{ap_expr=Case kees=:{case_guards=AlgebraicPatterns gi case_patterns,case_default=Yes default_expr}}:patterns]
		fun_def=:{fun_body = TransformedBody {tb_args,tb_rhs=tb_rhs}}
		rev_new_fun_defs next_fun_def_i next_group_number common_defs main_module_n expr_heap type_var_heap attr_var_heap var_heap
	# (updated_case_patterns,rev_new_fun_defs,next_fun_def_i,next_group_number,expr_heap,type_var_heap,attr_var_heap,var_heap)
		= create_new_gadt_type_functions_if_gadt_type gadt_types_case_patterns case_patterns (RootCaseAndPattern tb_rhs pattern) tb_args fun_def
			rev_new_fun_defs next_fun_def_i next_group_number common_defs main_module_n expr_heap type_var_heap attr_var_heap var_heap
	# (new_case_info_ptr,expr_heap) = newPtr EI_Empty expr_heap
	  pattern & ap_expr = Case {kees & case_guards=AlgebraicPatterns gi updated_case_patterns, case_info_ptr=new_case_info_ptr}
	# (patterns,rev_new_fun_defs,next_fun_def_i,next_group_number,expr_heap,type_var_heap,attr_var_heap,var_heap)
		= create_functions_for_gadt_case_alternatives_of_function gadt_types patterns
			fun_def rev_new_fun_defs next_fun_def_i next_group_number common_defs main_module_n expr_heap type_var_heap attr_var_heap var_heap
	= ([pattern:patterns],rev_new_fun_defs,next_fun_def_i,next_group_number,expr_heap,type_var_heap,attr_var_heap,var_heap)
create_functions_for_gadt_case_alternatives_of_function
		[!gadt_type_pattern=:GADTTypePatternWithGADTCaseWithGADTCaseInDefault (Yes _) gadt_types_case_patterns gadt_types_default:gadt_types!]
		[pattern=:{ap_expr=Case kees=:{case_guards=AlgebraicPatterns pattern_gi case_patterns,
		 case_default=Yes default_expr=:(Case default_case=:{case_guards=AlgebraicPatterns default_case_gi default_patterns})}}:patterns]
		fun_def=:{fun_body = TransformedBody {tb_args,tb_rhs}}
		rev_new_fun_defs next_fun_def_i next_group_number common_defs main_module_n expr_heap type_var_heap attr_var_heap var_heap
	# (updated_case_patterns,rev_new_fun_defs,next_fun_def_i,next_group_number,expr_heap,type_var_heap,attr_var_heap,var_heap)
		= create_new_gadt_type_functions gadt_types_case_patterns case_patterns (RootCaseAndPattern tb_rhs pattern) tb_args fun_def
			rev_new_fun_defs next_fun_def_i next_group_number common_defs main_module_n expr_heap type_var_heap attr_var_heap var_heap
	# (updated_default_patterns,rev_new_fun_defs,next_fun_def_i,next_group_number,expr_heap,type_var_heap,attr_var_heap,var_heap)
		= create_new_gadt_type_functions gadt_types_default default_patterns (RootCasePatternAndDefault tb_rhs pattern default_expr) tb_args fun_def
			rev_new_fun_defs next_fun_def_i next_group_number common_defs main_module_n expr_heap type_var_heap attr_var_heap var_heap
	# updated_default = Case {default_case & case_guards=AlgebraicPatterns default_case_gi updated_default_patterns}
	# pattern & ap_expr=Case {kees & case_guards=AlgebraicPatterns pattern_gi updated_case_patterns,case_default=Yes updated_default}
	# (patterns,rev_new_fun_defs,next_fun_def_i,next_group_number,expr_heap,type_var_heap,attr_var_heap,var_heap)
		= create_functions_for_gadt_case_alternatives_of_function gadt_types patterns
			fun_def rev_new_fun_defs next_fun_def_i next_group_number common_defs main_module_n expr_heap type_var_heap attr_var_heap var_heap
	= ([pattern:patterns],rev_new_fun_defs,next_fun_def_i,next_group_number,expr_heap,type_var_heap,attr_var_heap,var_heap)
create_functions_for_gadt_case_alternatives_of_function
		[!gadt_type_pattern=:GADTTypePatternWithGADTCaseWithGADTCaseInDefault No gadt_types_case_patterns gadt_types_default:gadt_types!]
		[pattern=:{ap_expr=Case kees=:{case_guards=AlgebraicPatterns pattern_gi case_patterns,
		 case_default=Yes default_expr=:(Case default_case=:{case_guards=AlgebraicPatterns default_case_gi default_patterns})}}:patterns]
		fun_def=:{fun_body = TransformedBody {tb_args,tb_rhs}}
		rev_new_fun_defs next_fun_def_i next_group_number common_defs main_module_n expr_heap type_var_heap attr_var_heap var_heap
	# (updated_case_patterns,rev_new_fun_defs,next_fun_def_i,next_group_number,expr_heap,type_var_heap,attr_var_heap,var_heap)
		= create_new_gadt_type_functions_if_gadt_type gadt_types_case_patterns case_patterns (RootCaseAndPattern tb_rhs pattern) tb_args fun_def
			rev_new_fun_defs next_fun_def_i next_group_number common_defs main_module_n expr_heap type_var_heap attr_var_heap var_heap
	# (updated_default_patterns,rev_new_fun_defs,next_fun_def_i,next_group_number,expr_heap,type_var_heap,attr_var_heap,var_heap)
		= create_new_gadt_type_functions_if_gadt_type gadt_types_default default_patterns (RootCasePatternAndDefault tb_rhs pattern default_expr) tb_args fun_def
			rev_new_fun_defs next_fun_def_i next_group_number common_defs main_module_n expr_heap type_var_heap attr_var_heap var_heap
	# updated_default = Case {default_case & case_guards=AlgebraicPatterns default_case_gi updated_default_patterns}
	# pattern & ap_expr=Case {kees & case_guards=AlgebraicPatterns pattern_gi updated_case_patterns,case_default=Yes updated_default}
	# (patterns,rev_new_fun_defs,next_fun_def_i,next_group_number,expr_heap,type_var_heap,attr_var_heap,var_heap)
		= create_functions_for_gadt_case_alternatives_of_function gadt_types patterns
			fun_def rev_new_fun_defs next_fun_def_i next_group_number common_defs main_module_n expr_heap type_var_heap attr_var_heap var_heap
	= ([pattern:patterns],rev_new_fun_defs,next_fun_def_i,next_group_number,expr_heap,type_var_heap,attr_var_heap,var_heap)
create_functions_for_gadt_case_alternatives_of_function
		[!KeepGADTPattern:gadt_types!] [pattern:patterns]
		fun_def rev_new_fun_defs next_fun_def_i next_group_number common_defs main_module_n expr_heap type_var_heap attr_var_heap var_heap
	# (patterns,rev_new_fun_defs,next_fun_def_i,next_group_number,expr_heap,type_var_heap,attr_var_heap,var_heap)
		= create_functions_for_gadt_case_alternatives_of_function
			gadt_types patterns fun_def rev_new_fun_defs next_fun_def_i next_group_number common_defs main_module_n
			expr_heap type_var_heap attr_var_heap var_heap
	= ([pattern:patterns],rev_new_fun_defs,next_fun_def_i,next_group_number,expr_heap,type_var_heap,attr_var_heap,var_heap)
create_functions_for_gadt_case_alternatives_of_function
		[!!] []
		fun_def rev_new_fun_defs next_fun_def_i next_group_number common_defs main_module_n expr_heap type_var_heap attr_var_heap var_heap
	= ([],rev_new_fun_defs,next_fun_def_i,next_group_number,expr_heap,type_var_heap,attr_var_heap,var_heap)

create_new_gadt_type_functions_if_gadt_type ::
	![!PatternsGADTTypes!] ![AlgebraicPattern] !CasesAndPatterns ![FreeVar] !FunDef ![!FunDef!] !Int !Int !{#CommonDefs} !Int
														!*ExpressionHeap !*TypeVarHeap !*AttrVarHeap !*VarHeap
	-> (![AlgebraicPattern],![!FunDef!],!Int,!Int,!*ExpressionHeap,!*TypeVarHeap,!*AttrVarHeap,!*VarHeap)
create_new_gadt_type_functions_if_gadt_type [!GADTTypePattern (Yes _):gadt_types!] [pattern:patterns] cases_and_patterns tb_args fun_def
		rev_new_fun_defs next_fun_def_i next_group_number common_defs main_module_n expr_heap type_var_heap attr_var_heap var_heap
	# (updated_pattern,rev_new_fun_defs,next_fun_def_i,next_group_number,expr_heap,type_var_heap,attr_var_heap,var_heap)
		= create_new_gadt_type_function pattern cases_and_patterns tb_args fun_def
			rev_new_fun_defs next_fun_def_i next_group_number common_defs main_module_n expr_heap type_var_heap attr_var_heap var_heap
	# (updated_patterns,rev_new_fun_defs,next_fun_def_i,next_group_number,expr_heap,type_var_heap,attr_var_heap,var_heap)
		= create_new_gadt_type_functions_if_gadt_type gadt_types patterns cases_and_patterns tb_args fun_def
			rev_new_fun_defs next_fun_def_i next_group_number common_defs main_module_n expr_heap type_var_heap attr_var_heap var_heap
	= ([updated_pattern:updated_patterns],rev_new_fun_defs,next_fun_def_i,next_group_number,expr_heap,type_var_heap,attr_var_heap,var_heap)
create_new_gadt_type_functions_if_gadt_type [!GADTTypePattern No:gadt_types!] [pattern:patterns] cases_and_patterns tb_args fun_def
		rev_new_fun_defs next_fun_def_i next_group_number common_defs main_module_n expr_heap type_var_heap attr_var_heap var_heap
	# (updated_patterns,rev_new_fun_defs,next_fun_def_i,next_group_number,expr_heap,type_var_heap,attr_var_heap,var_heap)
		= create_new_gadt_type_functions_if_gadt_type gadt_types patterns cases_and_patterns tb_args fun_def
			rev_new_fun_defs next_fun_def_i next_group_number common_defs main_module_n expr_heap type_var_heap attr_var_heap var_heap
	= ([pattern:updated_patterns],rev_new_fun_defs,next_fun_def_i,next_group_number,expr_heap,type_var_heap,attr_var_heap,var_heap)
create_new_gadt_type_functions_if_gadt_type [!GADTTypePatternWithGADTCase (Yes _) gadt_types_case_patterns:gadt_types!]
		[pattern=:{ap_expr=Case kees=:{case_guards=AlgebraicPatterns gi case_patterns}}:patterns] cases_and_patterns tb_args fun_def
		rev_new_fun_defs next_fun_def_i next_group_number common_defs main_module_n expr_heap type_var_heap attr_var_heap var_heap
	# (updated_case_patterns,rev_new_fun_defs,next_fun_def_i,next_group_number,expr_heap,type_var_heap,attr_var_heap,var_heap)
		= create_new_gadt_type_functions gadt_types_case_patterns case_patterns (CasesAndPatterns cases_and_patterns pattern) tb_args fun_def
			rev_new_fun_defs next_fun_def_i next_group_number common_defs main_module_n expr_heap type_var_heap attr_var_heap var_heap
	# (new_case_info_ptr,expr_heap) = newPtr EI_Empty expr_heap
	  updated_case = Case {kees & case_guards=AlgebraicPatterns gi updated_case_patterns, case_default=No, case_info_ptr=new_case_info_ptr}
	  updated_pattern = {pattern & ap_expr = updated_case}
	# (updated_patterns,rev_new_fun_defs,next_fun_def_i,next_group_number,expr_heap,type_var_heap,attr_var_heap,var_heap)
		= create_new_gadt_type_functions_if_gadt_type gadt_types patterns cases_and_patterns tb_args fun_def
			rev_new_fun_defs next_fun_def_i next_group_number common_defs main_module_n expr_heap type_var_heap attr_var_heap var_heap
	= ([updated_pattern:updated_patterns],rev_new_fun_defs,next_fun_def_i,next_group_number,expr_heap,type_var_heap,attr_var_heap,var_heap)
create_new_gadt_type_functions_if_gadt_type [!GADTTypePatternWithGADTCase No gadt_types_case_patterns:gadt_types!]
		[pattern=:{ap_expr=Case kees=:{case_guards=AlgebraicPatterns gi case_patterns}}:patterns] cases_and_patterns tb_args fun_def
		rev_new_fun_defs next_fun_def_i next_group_number common_defs main_module_n expr_heap type_var_heap attr_var_heap var_heap
	# (updated_case_patterns,rev_new_fun_defs,next_fun_def_i,next_group_number,expr_heap,type_var_heap,attr_var_heap,var_heap)
		= create_new_gadt_type_functions_if_gadt_type gadt_types_case_patterns case_patterns (CasesAndPatterns cases_and_patterns pattern) tb_args fun_def
			rev_new_fun_defs next_fun_def_i next_group_number common_defs main_module_n expr_heap type_var_heap attr_var_heap var_heap
	# (new_case_info_ptr,expr_heap) = newPtr EI_Empty expr_heap
	  updated_case = Case {kees & case_guards=AlgebraicPatterns gi updated_case_patterns, case_default=No, case_info_ptr=new_case_info_ptr}
	  updated_pattern = {pattern & ap_expr = updated_case}
	# (updated_patterns,rev_new_fun_defs,next_fun_def_i,next_group_number,expr_heap,type_var_heap,attr_var_heap,var_heap)
		= create_new_gadt_type_functions_if_gadt_type gadt_types patterns cases_and_patterns tb_args fun_def
			rev_new_fun_defs next_fun_def_i next_group_number common_defs main_module_n expr_heap type_var_heap attr_var_heap var_heap
	= ([updated_pattern:updated_patterns],rev_new_fun_defs,next_fun_def_i,next_group_number,expr_heap,type_var_heap,attr_var_heap,var_heap)
create_new_gadt_type_functions_if_gadt_type [!GADTTypePatternWithGADTCaseWithDefault (Yes _) gadt_types_case_patterns:gadt_types!]
		[pattern=:{ap_expr=Case kees=:{case_guards=AlgebraicPatterns _ case_patterns,case_default=Yes default_expr}}:patterns]
		cases_and_patterns tb_args fun_def
		rev_new_fun_defs next_fun_def_i next_group_number common_defs main_module_n expr_heap type_var_heap attr_var_heap var_heap
	# (updated_case_patterns,rev_new_fun_defs,next_fun_def_i,next_group_number,
			expr_heap,type_var_heap,attr_var_heap,var_heap)
		= create_new_gadt_type_functions gadt_types_case_patterns case_patterns (CasesAndPatterns cases_and_patterns pattern) tb_args fun_def
			rev_new_fun_defs next_fun_def_i next_group_number common_defs main_module_n expr_heap type_var_heap attr_var_heap var_heap
	# (updated_pattern,rev_new_fun_defs,next_fun_def_i,next_group_number,expr_heap,type_var_heap,attr_var_heap,var_heap)
		= create_new_gadt_type_function_for_default_and_updated_case_patterns updated_case_patterns pattern cases_and_patterns tb_args fun_def
			rev_new_fun_defs next_fun_def_i next_group_number common_defs main_module_n expr_heap type_var_heap attr_var_heap var_heap
	# (updated_patterns,rev_new_fun_defs,next_fun_def_i,next_group_number,expr_heap,type_var_heap,attr_var_heap,var_heap)
		= create_new_gadt_type_functions_if_gadt_type gadt_types patterns cases_and_patterns tb_args fun_def
			rev_new_fun_defs next_fun_def_i next_group_number common_defs main_module_n expr_heap type_var_heap attr_var_heap var_heap
	= ([updated_pattern:updated_patterns],rev_new_fun_defs,next_fun_def_i,next_group_number,expr_heap,type_var_heap,attr_var_heap,var_heap)
create_new_gadt_type_functions_if_gadt_type [!GADTTypePatternWithGADTCaseWithDefault No gadt_types_case_patterns:gadt_types!]
		[pattern=:{ap_expr=Case kees=:{case_guards=AlgebraicPatterns gi case_patterns,case_default=Yes default_expr}}:patterns]
		cases_and_patterns tb_args fun_def
		rev_new_fun_defs next_fun_def_i next_group_number common_defs main_module_n expr_heap type_var_heap attr_var_heap var_heap
	# (updated_case_patterns,rev_new_fun_defs,next_fun_def_i,next_group_number,expr_heap,type_var_heap,attr_var_heap,var_heap)
		= create_new_gadt_type_functions_if_gadt_type gadt_types_case_patterns case_patterns (CasesAndPatterns cases_and_patterns pattern) tb_args fun_def
			rev_new_fun_defs next_fun_def_i next_group_number common_defs main_module_n expr_heap type_var_heap attr_var_heap var_heap
	# (new_case_info_ptr,expr_heap) = newPtr EI_Empty expr_heap
	  updated_case = Case {kees & case_guards=AlgebraicPatterns gi updated_case_patterns, case_info_ptr=new_case_info_ptr}
	  updated_pattern = {pattern & ap_expr = updated_case}
	# (updated_patterns,rev_new_fun_defs,next_fun_def_i,next_group_number,expr_heap,type_var_heap,attr_var_heap,var_heap)
		= create_new_gadt_type_functions_if_gadt_type gadt_types patterns cases_and_patterns tb_args fun_def
			rev_new_fun_defs next_fun_def_i next_group_number common_defs main_module_n expr_heap type_var_heap attr_var_heap var_heap
	= ([updated_pattern:updated_patterns],rev_new_fun_defs,next_fun_def_i,next_group_number,expr_heap,type_var_heap,attr_var_heap,var_heap)
create_new_gadt_type_functions_if_gadt_type [!GADTTypePatternWithGADTCaseWithGADTCaseInDefault (Yes _) gadt_types_case_patterns gadt_types_default:gadt_types!]
		[pattern=:{ap_expr=Case kees=:{case_guards=AlgebraicPatterns gi case_patterns,
		 case_default=Yes default_expr=:(Case {case_guards=AlgebraicPatterns _ default_patterns})}}:patterns]
		cases_and_patterns tb_args fun_def
		rev_new_fun_defs next_fun_def_i next_group_number common_defs main_module_n expr_heap type_var_heap attr_var_heap var_heap
	# (updated_case_patterns,rev_new_fun_defs,next_fun_def_i,next_group_number,expr_heap,type_var_heap,attr_var_heap,var_heap)
		= create_new_gadt_type_functions gadt_types_case_patterns case_patterns (CasesAndPatternsAndDefault cases_and_patterns pattern default_expr) tb_args fun_def
			rev_new_fun_defs next_fun_def_i next_group_number common_defs main_module_n expr_heap type_var_heap attr_var_heap var_heap
	# (updated_default_patterns,rev_new_fun_defs,next_fun_def_i,next_group_number,expr_heap,type_var_heap,attr_var_heap,var_heap)
		= create_new_gadt_type_functions gadt_types_default default_patterns (CasesAndPatterns cases_and_patterns pattern) tb_args fun_def
			rev_new_fun_defs next_fun_def_i next_group_number common_defs main_module_n expr_heap type_var_heap attr_var_heap var_heap
	# (new_case_info_ptr,expr_heap) = newPtr EI_Empty expr_heap
	  updated_case = Case {kees & case_guards=AlgebraicPatterns gi updated_case_patterns, case_default=No, case_info_ptr=new_case_info_ptr}
	  updated_pattern = {pattern & ap_expr = updated_case}
	# (updated_patterns,rev_new_fun_defs,next_fun_def_i,next_group_number,expr_heap,type_var_heap,attr_var_heap,var_heap)
		= create_new_gadt_type_functions_if_gadt_type gadt_types patterns cases_and_patterns tb_args fun_def
			rev_new_fun_defs next_fun_def_i next_group_number common_defs main_module_n expr_heap type_var_heap attr_var_heap var_heap
	= ([updated_pattern:updated_patterns],rev_new_fun_defs,next_fun_def_i,next_group_number,expr_heap,type_var_heap,attr_var_heap,var_heap)
create_new_gadt_type_functions_if_gadt_type [!GADTTypePatternWithGADTCaseWithGADTCaseInDefault No gadt_types_case_patterns gadt_types_default:gadt_types!]
		[pattern=:{ap_expr=Case kees=:{case_guards=AlgebraicPatterns gi case_patterns,
		 case_default=Yes default_expr=:(Case {case_guards=AlgebraicPatterns _ default_patterns})}}:patterns]
		cases_and_patterns tb_args fun_def
		rev_new_fun_defs next_fun_def_i next_group_number common_defs main_module_n expr_heap type_var_heap attr_var_heap var_heap
	# (updated_case_patterns,rev_new_fun_defs,next_fun_def_i,next_group_number,expr_heap,type_var_heap,attr_var_heap,var_heap)
		= create_new_gadt_type_functions_if_gadt_type gadt_types_case_patterns case_patterns (CasesAndPatternsAndDefault cases_and_patterns pattern default_expr) tb_args fun_def
			rev_new_fun_defs next_fun_def_i next_group_number common_defs main_module_n expr_heap type_var_heap attr_var_heap var_heap
	# (updated_default_patterns,rev_new_fun_defs,next_fun_def_i,next_group_number,expr_heap,type_var_heap,attr_var_heap,var_heap)
		= create_new_gadt_type_functions_if_gadt_type gadt_types_default default_patterns (CasesAndPatterns cases_and_patterns pattern) tb_args fun_def
			rev_new_fun_defs next_fun_def_i next_group_number common_defs main_module_n expr_heap type_var_heap attr_var_heap var_heap
	# (new_case_info_ptr,expr_heap) = newPtr EI_Empty expr_heap
	  updated_case = Case {kees & case_guards=AlgebraicPatterns gi updated_case_patterns, case_default=No, case_info_ptr=new_case_info_ptr}
	  updated_pattern = {pattern & ap_expr = updated_case}
	# (updated_patterns,rev_new_fun_defs,next_fun_def_i,next_group_number,expr_heap,type_var_heap,attr_var_heap,var_heap)
		= create_new_gadt_type_functions_if_gadt_type gadt_types patterns cases_and_patterns tb_args fun_def
			rev_new_fun_defs next_fun_def_i next_group_number common_defs main_module_n expr_heap type_var_heap attr_var_heap var_heap
	= ([updated_pattern:updated_patterns],rev_new_fun_defs,next_fun_def_i,next_group_number,expr_heap,type_var_heap,attr_var_heap,var_heap)
create_new_gadt_type_functions_if_gadt_type [!KeepGADTPattern:gadt_types!] [pattern:patterns] cases_and_patterns tb_args fun_def
		rev_new_fun_defs next_fun_def_i next_group_number common_defs main_module_n expr_heap type_var_heap attr_var_heap var_heap
	# (updated_patterns,rev_new_fun_defs,next_fun_def_i,next_group_number,expr_heap,type_var_heap,attr_var_heap,var_heap)
		= create_new_gadt_type_functions_if_gadt_type gadt_types patterns cases_and_patterns tb_args fun_def
			rev_new_fun_defs next_fun_def_i next_group_number common_defs main_module_n expr_heap type_var_heap attr_var_heap var_heap
	= ([pattern:updated_patterns],rev_new_fun_defs,next_fun_def_i,next_group_number,expr_heap,type_var_heap,attr_var_heap,var_heap)
create_new_gadt_type_functions_if_gadt_type [!!] [] cases_and_patterns tb_args fun_def
		rev_new_fun_defs next_fun_def_i next_group_number common_defs main_module_n expr_heap type_var_heap attr_var_heap var_heap
	= ([],rev_new_fun_defs,next_fun_def_i,next_group_number,expr_heap,type_var_heap,attr_var_heap,var_heap)

create_new_gadt_type_functions ::
	![!PatternsGADTTypes!] ![AlgebraicPattern] !CasesAndPatterns ![FreeVar] !FunDef ![!FunDef!] !Int !Int !{#CommonDefs} !Int
														!*ExpressionHeap !*TypeVarHeap !*AttrVarHeap !*VarHeap
	-> (![AlgebraicPattern],![!FunDef!],!Int,!Int,!*ExpressionHeap,!*TypeVarHeap,!*AttrVarHeap,!*VarHeap)
create_new_gadt_type_functions [!GADTTypePattern _:gadt_types!] [pattern:patterns] cases_and_patterns tb_args fun_def
		rev_new_fun_defs next_fun_def_i next_group_number common_defs main_module_n expr_heap type_var_heap attr_var_heap var_heap
	# (updated_pattern,rev_new_fun_defs,next_fun_def_i,next_group_number,expr_heap,type_var_heap,attr_var_heap,var_heap)
		= create_new_gadt_type_function pattern cases_and_patterns tb_args fun_def
			rev_new_fun_defs next_fun_def_i next_group_number common_defs main_module_n expr_heap type_var_heap attr_var_heap var_heap
	# (updated_patterns,rev_new_fun_defs,next_fun_def_i,next_group_number,expr_heap,type_var_heap,attr_var_heap,var_heap)
		= create_new_gadt_type_functions gadt_types patterns cases_and_patterns tb_args fun_def
			rev_new_fun_defs next_fun_def_i next_group_number common_defs main_module_n expr_heap type_var_heap attr_var_heap var_heap
	= ([updated_pattern:updated_patterns],rev_new_fun_defs,next_fun_def_i,next_group_number,expr_heap,type_var_heap,attr_var_heap,var_heap)
create_new_gadt_type_functions [!GADTTypePatternWithGADTCase _ gadt_types_case_patterns:gadt_types!]
		[pattern=:{ap_expr=Case kees=:{case_guards=AlgebraicPatterns gi case_patterns}}:patterns] cases_and_patterns tb_args fun_def
		rev_new_fun_defs next_fun_def_i next_group_number common_defs main_module_n expr_heap type_var_heap attr_var_heap var_heap
	# (updated_case_patterns,rev_new_fun_defs,next_fun_def_i,next_group_number,expr_heap,type_var_heap,attr_var_heap,var_heap)
		= create_new_gadt_type_functions gadt_types_case_patterns case_patterns (CasesAndPatterns cases_and_patterns pattern) tb_args fun_def
			rev_new_fun_defs next_fun_def_i next_group_number common_defs main_module_n expr_heap type_var_heap attr_var_heap var_heap
	# (new_case_info_ptr,expr_heap) = newPtr EI_Empty expr_heap
	  updated_case = Case {kees & case_guards=AlgebraicPatterns gi updated_case_patterns, case_default=No, case_info_ptr=new_case_info_ptr}
	  updated_pattern = {pattern & ap_expr = updated_case}
	# (updated_patterns,rev_new_fun_defs,next_fun_def_i,next_group_number,expr_heap,type_var_heap,attr_var_heap,var_heap)
		= create_new_gadt_type_functions gadt_types patterns cases_and_patterns tb_args fun_def
			rev_new_fun_defs next_fun_def_i next_group_number common_defs main_module_n expr_heap type_var_heap attr_var_heap var_heap
	= ([updated_pattern:updated_patterns],rev_new_fun_defs,next_fun_def_i,next_group_number,expr_heap,type_var_heap,attr_var_heap,var_heap)
create_new_gadt_type_functions [!GADTTypePatternWithGADTCaseWithDefault _ gadt_types_case_patterns:gadt_types!]
		[pattern=:{ap_expr=Case kees=:{case_guards=AlgebraicPatterns _ case_patterns,case_default=Yes default_expr}}:patterns]
		cases_and_patterns tb_args fun_def
		rev_new_fun_defs next_fun_def_i next_group_number common_defs main_module_n expr_heap type_var_heap attr_var_heap var_heap
	# (updated_case_patterns,rev_new_fun_defs,next_fun_def_i,next_group_number,expr_heap,type_var_heap,attr_var_heap,var_heap)
		= create_new_gadt_type_functions gadt_types_case_patterns case_patterns (CasesAndPatterns cases_and_patterns pattern) tb_args fun_def
			rev_new_fun_defs next_fun_def_i next_group_number common_defs main_module_n expr_heap type_var_heap attr_var_heap var_heap
	# (updated_pattern,rev_new_fun_defs,next_fun_def_i,next_group_number,expr_heap,type_var_heap,attr_var_heap,var_heap)
		= create_new_gadt_type_function_for_default_and_updated_case_patterns updated_case_patterns pattern cases_and_patterns tb_args fun_def
			rev_new_fun_defs next_fun_def_i next_group_number common_defs main_module_n expr_heap type_var_heap attr_var_heap var_heap
	# (updated_patterns,rev_new_fun_defs,next_fun_def_i,next_group_number,expr_heap,type_var_heap,attr_var_heap,var_heap)
		= create_new_gadt_type_functions gadt_types patterns cases_and_patterns tb_args fun_def
			rev_new_fun_defs next_fun_def_i next_group_number common_defs main_module_n expr_heap type_var_heap attr_var_heap var_heap
	= ([updated_pattern:updated_patterns],rev_new_fun_defs,next_fun_def_i,next_group_number,expr_heap,type_var_heap,attr_var_heap,var_heap)
create_new_gadt_type_functions [!GADTTypePatternWithGADTCaseWithGADTCaseInDefault _ gadt_types_case_patterns gadt_types_default:gadt_types!]
		[pattern=:{ap_expr=Case kees=:{case_guards=AlgebraicPatterns gi case_patterns,
		 case_default=Yes default_expr=:(Case {case_guards=AlgebraicPatterns _ default_patterns})}}:patterns]
		cases_and_patterns tb_args fun_def
		rev_new_fun_defs next_fun_def_i next_group_number common_defs main_module_n expr_heap type_var_heap attr_var_heap var_heap
	# (updated_case_patterns,rev_new_fun_defs,next_fun_def_i,next_group_number,expr_heap,type_var_heap,attr_var_heap,var_heap)
		= create_new_gadt_type_functions gadt_types_case_patterns case_patterns (CasesAndPatternsAndDefault cases_and_patterns pattern default_expr) tb_args fun_def
			rev_new_fun_defs next_fun_def_i next_group_number common_defs main_module_n expr_heap type_var_heap attr_var_heap var_heap
	# (updated_default_patterns,rev_new_fun_defs,next_fun_def_i,next_group_number,expr_heap,type_var_heap,attr_var_heap,var_heap)
		= create_new_gadt_type_functions gadt_types_default default_patterns (CasesAndPatterns cases_and_patterns pattern) tb_args fun_def
			rev_new_fun_defs next_fun_def_i next_group_number common_defs main_module_n expr_heap type_var_heap attr_var_heap var_heap
	# (new_case_info_ptr,expr_heap) = newPtr EI_Empty expr_heap
	  updated_case = Case {kees & case_guards=AlgebraicPatterns gi updated_case_patterns, case_default=No, case_info_ptr=new_case_info_ptr}
	  updated_pattern = {pattern & ap_expr = updated_case}
	# (updated_patterns,rev_new_fun_defs,next_fun_def_i,next_group_number,expr_heap,type_var_heap,attr_var_heap,var_heap)
		= create_new_gadt_type_functions gadt_types patterns cases_and_patterns tb_args fun_def
			rev_new_fun_defs next_fun_def_i next_group_number common_defs main_module_n expr_heap type_var_heap attr_var_heap var_heap
	= ([updated_pattern:updated_patterns],rev_new_fun_defs,next_fun_def_i,next_group_number,expr_heap,type_var_heap,attr_var_heap,var_heap)
create_new_gadt_type_functions [!KeepGADTPattern:gadt_types!] [pattern:patterns] cases_and_patterns tb_args fun_def
		rev_new_fun_defs next_fun_def_i next_group_number common_defs main_module_n expr_heap type_var_heap attr_var_heap var_heap
	# (updated_patterns,rev_new_fun_defs,next_fun_def_i,next_group_number,expr_heap,type_var_heap,attr_var_heap,var_heap)
		= create_new_gadt_type_functions gadt_types patterns cases_and_patterns tb_args fun_def
			rev_new_fun_defs next_fun_def_i next_group_number common_defs main_module_n expr_heap type_var_heap attr_var_heap var_heap
	= ([pattern:updated_patterns],rev_new_fun_defs,next_fun_def_i,next_group_number,expr_heap,type_var_heap,attr_var_heap,var_heap)
create_new_gadt_type_functions [!!] [] cases_and_patterns tb_args fun_def
		rev_new_fun_defs next_fun_def_i next_group_number common_defs main_module_n expr_heap type_var_heap attr_var_heap var_heap
	= ([],rev_new_fun_defs,next_fun_def_i,next_group_number,expr_heap,type_var_heap,attr_var_heap,var_heap)

create_new_gadt_type_function ::
	!AlgebraicPattern !CasesAndPatterns ![FreeVar] !FunDef ![!FunDef!] !Int !Int !{#CommonDefs} !Int
														!*ExpressionHeap !*TypeVarHeap !*AttrVarHeap !*VarHeap
	-> (!AlgebraicPattern,![!FunDef!],!Int,!Int,!*ExpressionHeap,!*TypeVarHeap,!*AttrVarHeap,!*VarHeap)

create_new_gadt_type_function pattern cases_and_patterns tb_args fun_def=:{fun_type,fun_info}
		rev_new_fun_defs next_fun_def_i next_group_number common_defs main_module_n expr_heap type_var_heap attr_var_heap var_heap
	# (fun_type_copy, {cs_type_var_heap=type_var_heap, cs_attr_var_heap=attr_var_heap, cs_var_heap=var_heap})
		= copy_function_type fun_type common_defs {cs_type_var_heap=type_var_heap, cs_attr_var_heap=attr_var_heap, cs_var_heap=var_heap}

	  new_fun_def_i=next_fun_def_i
	  new_group_number=next_group_number
	  next_fun_def_i=next_fun_def_i+1
	  next_group_number=next_group_number+1

	  app_symb = {symb_ident=fun_def.fun_ident,symb_kind=SK_Function {glob_module=main_module_n,glob_object=new_fun_def_i}}

	  (new_rhs,expr_heap,var_heap) = copy_cases_and_patterns cases_and_patterns pattern expr_heap var_heap

	  (new_args,var_heap) = create_new_args_and_forward_old_args tb_args var_heap
	  (fun_body,local_vars,fun_calls,dynamics,expr_heap,var_heap)
		= replace_variables_determine_ref_counts_and_calls new_args new_rhs expr_heap var_heap

	  fun_info & fi_group_index=new_group_number, fi_calls=fun_calls, fi_local_vars=local_vars, fi_dynamics=dynamics
	  new_fun_def = {fun_def & fun_type = fun_type_copy, fun_body = fun_body, fun_info = fun_info, fun_kind = FK_Function False}
	  rev_new_fun_defs = [!new_fun_def:rev_new_fun_defs!]

	  (app_expr,expr_heap) = call_function app_symb tb_args expr_heap
	  updated_pattern = {pattern & ap_expr = app_expr}

	= (updated_pattern,rev_new_fun_defs,next_fun_def_i,next_group_number,expr_heap,type_var_heap,attr_var_heap,var_heap)

create_new_gadt_type_function_for_default_and_updated_case_patterns ::
	![AlgebraicPattern] !AlgebraicPattern !CasesAndPatterns ![FreeVar] !FunDef ![!FunDef!] !Int !Int !{#CommonDefs} !Int
														!*ExpressionHeap !*TypeVarHeap !*AttrVarHeap !*VarHeap
	-> (!AlgebraicPattern,![!FunDef!],!Int,!Int,!*ExpressionHeap,!*TypeVarHeap,!*AttrVarHeap,!*VarHeap)

create_new_gadt_type_function_for_default_and_updated_case_patterns updated_case_patterns
		pattern=:{ap_expr=Case kees=:{case_guards=AlgebraicPatterns gi _,case_default=Yes default_expr}}
		cases_and_patterns tb_args fun_def=:{fun_type,fun_info}
		rev_new_fun_defs next_fun_def_i next_group_number common_defs main_module_n expr_heap type_var_heap attr_var_heap var_heap
	# (fun_type_copy, {cs_type_var_heap=type_var_heap, cs_attr_var_heap=attr_var_heap, cs_var_heap=var_heap})
		= copy_function_type fun_type common_defs {cs_type_var_heap=type_var_heap, cs_attr_var_heap=attr_var_heap, cs_var_heap=var_heap}

	  new_fun_def_i=next_fun_def_i
	  new_group_number=next_group_number
	  next_fun_def_i=next_fun_def_i+1
	  next_group_number=next_group_number+1

	  app_symb = {symb_ident=fun_def.fun_ident,symb_kind=SK_Function {glob_module=main_module_n,glob_object=new_fun_def_i}}

	  new_pattern = {pattern & ap_expr=default_expr}

	  (new_rhs,expr_heap,var_heap) = copy_cases_and_patterns cases_and_patterns new_pattern expr_heap var_heap

	  (new_args,var_heap) = create_new_args_and_forward_old_args tb_args var_heap
	  (fun_body,local_vars,fun_calls,dynamics,expr_heap,var_heap)
		= replace_variables_determine_ref_counts_and_calls new_args new_rhs expr_heap var_heap

	  fun_info & fi_group_index=new_group_number, fi_calls=fun_calls, fi_local_vars=local_vars, fi_dynamics=dynamics
	  new_fun_def = {fun_def & fun_type = fun_type_copy, fun_body = fun_body, fun_info = fun_info, fun_kind = FK_Function False}
	  rev_new_fun_defs = [!new_fun_def:rev_new_fun_defs!]

	  (app_expr,expr_heap) = call_function app_symb tb_args expr_heap

	  (new_case_info_ptr,expr_heap) = newPtr EI_Empty expr_heap
	  updated_case = Case {kees & case_guards=AlgebraicPatterns gi updated_case_patterns, case_default=Yes app_expr, case_info_ptr=new_case_info_ptr}
	  updated_pattern = {pattern & ap_expr = updated_case}

	= (updated_pattern,rev_new_fun_defs,next_fun_def_i,next_group_number,expr_heap,type_var_heap,attr_var_heap,var_heap)

copy_cases_and_patterns :: !CasesAndPatterns !AlgebraicPattern !*ExpressionHeap !*VarHeap -> (!Expression,!*ExpressionHeap,!*VarHeap)
copy_cases_and_patterns (RootCase (Case kase=:{case_guards=AlgebraicPatterns gi _})) new_pattern expr_heap var_heap
	# (new_case_info_ptr,expr_heap) = newPtr EI_Empty expr_heap
	  expr = Case {kase & case_guards=AlgebraicPatterns gi [new_pattern], case_default=No, case_info_ptr=new_case_info_ptr}
	= (expr, expr_heap, var_heap)
copy_cases_and_patterns (RootCaseAndDefault (Case kase=:{case_guards=AlgebraicPatterns gi _}) default_expr) new_pattern expr_heap var_heap
	# (new_ap_vars,var_heap) = create_new_args_and_forward_old_args new_pattern.ap_vars var_heap
	  new_pattern & ap_vars=new_ap_vars, ap_expr=default_expr
	  (new_case_info_ptr,expr_heap) = newPtr EI_Empty expr_heap
	  expr = Case {kase & case_guards=AlgebraicPatterns gi [new_pattern], case_default=No, case_info_ptr=new_case_info_ptr}
	= (expr, expr_heap, var_heap)
copy_cases_and_patterns (RootCaseAndPattern (Case kase=:{case_guards=AlgebraicPatterns gi _})
							parent_pattern=:{ap_expr=Case kees=:{case_guards=AlgebraicPatterns pattern_gi _}}) new_pattern expr_heap var_heap
	# (new_ap_vars,var_heap) = create_new_args_and_forward_old_args parent_pattern.ap_vars var_heap
	  (new_case_info_ptr,expr_heap) = newPtr EI_Empty expr_heap
	  new_parent_pattern = {parent_pattern & ap_vars=new_ap_vars, ap_expr=Case {kees & case_guards=AlgebraicPatterns pattern_gi [new_pattern], case_default=No, case_info_ptr=new_case_info_ptr}}
	  (new_case_info_ptr,expr_heap) = newPtr EI_Empty expr_heap
	  expr = Case {kase & case_guards=AlgebraicPatterns gi [new_parent_pattern], case_default=No, case_info_ptr=new_case_info_ptr}
	= (expr, expr_heap, var_heap)
copy_cases_and_patterns (RootCasePatternAndDefault (Case kase=:{case_guards=AlgebraicPatterns gi _})
							parent_pattern (Case kees=:{case_guards=AlgebraicPatterns pattern_gi _})) new_pattern expr_heap var_heap
	# (new_ap_vars,var_heap) = create_new_args_and_forward_old_args parent_pattern.ap_vars var_heap
	  (new_case_info_ptr,expr_heap) = newPtr EI_Empty expr_heap
	  new_parent_pattern = {parent_pattern & ap_vars=new_ap_vars, ap_expr=Case {kees & case_guards=AlgebraicPatterns pattern_gi [new_pattern], case_default=No, case_info_ptr=new_case_info_ptr}}
	  (new_case_info_ptr,expr_heap) = newPtr EI_Empty expr_heap
	  expr = Case {kase & case_guards=AlgebraicPatterns gi [new_parent_pattern], case_default=No, case_info_ptr=new_case_info_ptr}
	= (expr, expr_heap, var_heap)
copy_cases_and_patterns (CasesAndPatterns cases_and_patterns
							parent_pattern=:{ap_expr=Case kees=:{case_guards=AlgebraicPatterns pattern_gi _}}) new_pattern expr_heap var_heap
	# (new_ap_vars,var_heap) = create_new_args_and_forward_old_args parent_pattern.ap_vars var_heap
	  (new_case_info_ptr,expr_heap) = newPtr EI_Empty expr_heap
	  new_parent_pattern = {parent_pattern & ap_vars=new_ap_vars, ap_expr=Case {kees & case_guards=AlgebraicPatterns pattern_gi [new_pattern], case_default=No, case_info_ptr=new_case_info_ptr}}
	= copy_cases_and_patterns cases_and_patterns new_parent_pattern expr_heap var_heap
copy_cases_and_patterns (CasesAndPatternsAndDefault cases_and_patterns
							parent_pattern (Case kees=:{case_guards=AlgebraicPatterns pattern_gi _})) new_pattern expr_heap var_heap
	# (new_ap_vars,var_heap) = create_new_args_and_forward_old_args parent_pattern.ap_vars var_heap
	  (new_case_info_ptr,expr_heap) = newPtr EI_Empty expr_heap
	  new_parent_pattern = {parent_pattern & ap_vars=new_ap_vars, ap_expr=Case {kees & case_guards=AlgebraicPatterns pattern_gi [new_pattern], case_default=No, case_info_ptr=new_case_info_ptr}}
	= copy_cases_and_patterns cases_and_patterns new_parent_pattern expr_heap var_heap

call_function :: !SymbIdent ![FreeVar] !*ExpressionHeap -> (!Expression,!*ExpressionHeap)
call_function app_symb lhs_args expr_heap
	# (app_args,expr_heap) = free_vars_to_expressions lhs_args expr_heap
	  (app_info_ptr,expr_heap) = newPtr EI_Empty expr_heap
	= (App {app_symb=app_symb, app_args=app_args, app_info_ptr=app_info_ptr},expr_heap)
where
	free_vars_to_expressions [{fv_ident,fv_info_ptr}:free_vars] expr_heap
		# (var_expr_ptr,expr_heap) = newPtr EI_Empty expr_heap
		  bound_var = {var_ident=fv_ident,var_info_ptr=fv_info_ptr,var_expr_ptr=var_expr_ptr}
		  (expressions,expr_heap) = free_vars_to_expressions free_vars expr_heap
		= ([Var bound_var:expressions],expr_heap)
	free_vars_to_expressions [] expr_heap
		= ([],expr_heap)

copy_new_type_variable :: !TypeVar !*TypeVarHeap -> (!TypeVar,!*TypeVarHeap)
copy_new_type_variable {tv_ident,tv_info_ptr} type_var_heap
	# (new_tv_info_ptr,type_var_heap) = newPtr TVI_Empty type_var_heap
	  type_var_copy = {tv_ident=tv_ident,tv_info_ptr=new_tv_info_ptr}
	  type_var_heap = writePtr tv_info_ptr (TVI_Type (TV type_var_copy)) type_var_heap
	= (type_var_copy,type_var_heap)

copy_new_attribute_variable :: !AttributeVar !*AttrVarHeap -> (!AttributeVar,!*AttrVarHeap)
copy_new_attribute_variable {av_ident,av_info_ptr} attr_var_heap
	# (new_av_info_ptr,attr_var_heap) = newPtr AVI_Empty attr_var_heap
	  copy_attr_var = {av_ident=av_ident,av_info_ptr=new_av_info_ptr}
	  attr_var_heap = writePtr av_info_ptr (AVI_Attr (TA_Var copy_attr_var)) attr_var_heap
	= (copy_attr_var,attr_var_heap)

copy_new_type_attribute :: !TypeAttribute !*AttrVarHeap -> (!TypeAttribute,!*AttrVarHeap)
copy_new_type_attribute (TA_Var attr_var) attr_var_heap
	# (copy_attr_var,attr_var_heap) = copy_new_attribute_variable attr_var attr_var_heap
	= (TA_Var copy_attr_var,attr_var_heap)
copy_new_type_attribute (TA_RootVar attr_var) attr_var_heap
	# (copy_attr_var,attr_var_heap) = copy_new_attribute_variable attr_var attr_var_heap
	= (TA_RootVar copy_attr_var,attr_var_heap)
copy_new_type_attribute type_attr attr_var_heap
	= (type_attr,attr_var_heap)

copy_new_atype_variables :: ![ATypeVar] !*CopyState -> (![ATypeVar],!*CopyState)
copy_new_atype_variables [{atv_attribute,atv_variable}:atype_vars] cs=:{cs_type_var_heap,cs_attr_var_heap}
	# (copy_attribute,attr_var_heap) = copy_new_type_attribute atv_attribute cs_attr_var_heap
	  (type_var_copy,type_var_heap) = copy_new_type_variable atv_variable cs_type_var_heap
	  cs & cs_type_var_heap=type_var_heap,cs_attr_var_heap=attr_var_heap
	  (copy_atype_vars,cs) = copy_new_atype_variables atype_vars cs
	= ([{atv_attribute=atv_attribute,atv_variable=type_var_copy}:copy_atype_vars],cs)
copy_new_atype_variables [] cs
	= ([],cs)

copy_function_type :: !FunDefType !{#CommonDefs} !*CopyState -> (!FunDefType,!*CopyState)
copy_function_type (FunDefType st=:{st_vars,st_args,st_result,st_context,st_attr_vars,st_attr_env,st_arity}) common_defs
				cs=:{cs_type_var_heap,cs_attr_var_heap}
	# (vars_copy,type_var_heap) = copy_new_type_variables st_vars cs_type_var_heap
	  (attr_vars_copy,attr_var_heap) = copy_new_attribute_variables st_attr_vars cs_attr_var_heap
	  (attr_env_copy,attr_var_heap) = copy_attribute_environment st_attr_env attr_var_heap
	  cs & cs_type_var_heap=type_var_heap, cs_attr_var_heap=attr_var_heap
	  (args_copy,cs) = copy_atypes st_args cs
	  (result_copy,cs) = copy_atype st_result cs
	  (context_copy,cs) = copy_type_contexts st_context cs
	  type_var_heap = clear_type_variables st_vars cs.cs_type_var_heap
	  attr_var_heap = clear_attribute_variables st_attr_vars cs.cs_attr_var_heap
	  cs & cs_type_var_heap=type_var_heap, cs_attr_var_heap=attr_var_heap
	  st & st_vars=vars_copy, st_attr_vars=attr_vars_copy, st_attr_env=attr_env_copy,
		   st_args=args_copy, st_result=result_copy, st_context=context_copy
	= (FunDefType st,cs)
where
	copy_new_type_variables :: ![TypeVar] !*TypeVarHeap -> (![TypeVar],!*TypeVarHeap)
	copy_new_type_variables type_vars type_var_heap
		= mapSt copy_new_type_variable type_vars type_var_heap

	copy_new_attribute_variables :: ![AttributeVar] !*AttrVarHeap -> (![AttributeVar],!*AttrVarHeap)
	copy_new_attribute_variables attr_vars attr_var_heap
		= mapSt copy_new_attribute_variable attr_vars attr_var_heap

	copy_attribute_environment :: ![AttrInequality] !*AttrVarHeap -> (![AttrInequality],!*AttrVarHeap)
	copy_attribute_environment ineqs attr_heap
		= mapSt copy_inequality ineqs attr_heap

	copy_inequality :: !AttrInequality !*AttrVarHeap -> (!AttrInequality,!*AttrVarHeap)
	copy_inequality {ai_demanded,ai_offered} attr_heap
		# (av_dem_info, attr_heap) = readPtr ai_demanded.av_info_ptr attr_heap
		  (av_off_info, attr_heap) = readPtr ai_offered.av_info_ptr attr_heap
		  (AVI_Attr (TA_Var dem_attr_var)) = av_dem_info
		  (AVI_Attr (TA_Var off_attr_var)) = av_off_info
		= ({ai_demanded=dem_attr_var, ai_offered=off_attr_var}, attr_heap)

copy_type_attribute :: !TypeAttribute !*AttrVarHeap -> (!TypeAttribute,!*AttrVarHeap)
copy_type_attribute (TA_Var avar) attr_var_heap
	= copy_attribute_var avar attr_var_heap
copy_type_attribute (TA_RootVar avar) attr_var_heap
	= copy_attribute_var avar attr_var_heap
copy_type_attribute attr attr_var_heap
	= (attr, attr_var_heap)

copy_attribute_var :: !AttributeVar !*AttrVarHeap -> (!TypeAttribute,!*AttrVarHeap)
copy_attribute_var {av_info_ptr} attr_var_heap
	# (AVI_Attr attr, attr_var_heap) = readPtr av_info_ptr attr_var_heap
	= (attr, attr_var_heap)

copy_type_variable :: !TypeVar !*CopyState -> (!Type,!*CopyState)
copy_type_variable {tv_info_ptr} cs=:{cs_type_var_heap}
	# (TVI_Type copy_var,type_var_heap) = readPtr tv_info_ptr cs_type_var_heap
	= (copy_var, {cs & cs_type_var_heap=type_var_heap})

copy_cons_variable :: !TypeVar !*TypeVarHeap -> (!ConsVariable,!*TypeVarHeap)
copy_cons_variable {tv_info_ptr} type_var_heap
	# (TVI_Type (TV var), type_var_heap) = readPtr tv_info_ptr type_var_heap
	= (CV var, type_var_heap)

copy_atype :: !AType !*CopyState -> (!AType,!*CopyState)
copy_atype type=:{at_type, at_attribute} cs=:{cs_attr_var_heap}
	# (copy_attribute,attr_var_heap) = copy_type_attribute at_attribute cs_attr_var_heap
	  (type_copy,cs) = copy_type at_type {cs & cs_attr_var_heap=attr_var_heap}
	= ({at_type=type_copy, at_attribute=copy_attribute}, cs)

copy_type :: !Type !*CopyState -> (!Type,!*CopyState)
copy_type (TV tv) cs
	= copy_type_variable tv cs
copy_type (TA cons_id cons_args) cs
	# (args_copy, cs) = copy_atypes cons_args cs
	= (TA cons_id args_copy, cs)
copy_type (TAS cons_id cons_args strictness)  cs
	# (args_copy, cs) = copy_atypes cons_args cs
	= (TAS cons_id args_copy strictness, cs)
copy_type (arg_type --> res_type) cs
	# (arg_type, cs) = copy_atype arg_type cs
	  (res_type, cs) = copy_atype res_type cs
	= (arg_type --> res_type, cs)
copy_type (CV tv :@: atypes) cs
	# (atypes_copy, cs) = copy_atypes atypes cs
	  (cons_var_copy, type_var_heap) = copy_cons_variable tv cs.cs_type_var_heap
	= (cons_var_copy :@: atypes_copy, {cs & cs_type_var_heap=type_var_heap})
copy_type (TArrow1 arg_type) cs
	# (arg_type, cs) = copy_atype arg_type cs
	= (TArrow1 arg_type, cs)
copy_type (TFA vars type) cs
	# (vars_copy, cs) = copy_new_atype_variables vars cs
	  (type_copy, cs) = copy_type type cs
	  cs = clear_atype_variables vars cs
	= (TFA vars_copy type_copy, cs)
copy_type (TFAC vars type context) cs
	# (vars_copy, cs) = copy_new_atype_variables vars cs
	  (type_copy, cs) = copy_type type cs
	  (context_copy,cs) = copy_type_contexts context cs
	  cs = clear_atype_variables vars cs
	= (TFAC vars_copy type_copy context_copy, cs)
copy_type type cs
	= (type, cs)

copy_types :: ![Type] !*CopyState -> (![Type],!*CopyState)
copy_types l ls
	= mapSt copy_type l ls

copy_atypes :: ![AType] !*CopyState -> (![AType],!*CopyState)
copy_atypes l ls
	= mapSt copy_atype l ls

copy_type_contexts :: ![TypeContext] !*CopyState -> (![TypeContext],!*CopyState)
copy_type_contexts tcs cs
	= mapSt copy_type_context tcs cs
where
	copy_type_context :: !TypeContext !*CopyState -> (!TypeContext,!*CopyState)
	copy_type_context tc=:{tc_types} cs
		# (types_copy, cs) = copy_types tc_types cs
		  (new_var_info_ptr, var_heap) = newPtr VI_Empty cs.cs_var_heap
		  cs & cs_var_heap=var_heap
		= ({tc & tc_types=types_copy, tc_var=new_var_info_ptr}, cs)

clear_type_variable :: !TypeVar !*TypeVarHeap -> *TypeVarHeap
clear_type_variable {tv_info_ptr} var_heap = writePtr tv_info_ptr TVI_Empty var_heap

clear_type_variables :: ![TypeVar] !*TypeVarHeap -> *TypeVarHeap
clear_type_variables type_vars var_heap
	= foldSt clear_type_variable type_vars var_heap

clear_attribute_variable :: !AttributeVar !*AttrVarHeap -> *AttrVarHeap
clear_attribute_variable {av_info_ptr} attr_heap = writePtr av_info_ptr AVI_Empty attr_heap

clear_attribute_variables :: ![AttributeVar] !*AttrVarHeap -> *AttrVarHeap
clear_attribute_variables attr_vars attr_heap
	= foldSt clear_attribute_variable attr_vars attr_heap

clear_type_attribute :: !TypeAttribute !*AttrVarHeap -> *AttrVarHeap
clear_type_attribute (TA_Var attr_var) attr_heap = clear_attribute_variable attr_var attr_heap
clear_type_attribute (TA_RootVar attr_var) attr_heap = clear_attribute_variable attr_var attr_heap
clear_type_attribute _ attr_heap = attr_heap

clear_atype_variables :: ![ATypeVar] !*CopyState -> *CopyState
clear_atype_variables [{atv_attribute,atv_variable}:atype_vars] cs=:{cs_type_var_heap,cs_attr_var_heap}
	# attr_var_heap = clear_type_attribute atv_attribute cs_attr_var_heap
	  type_var_heap = clear_type_variable atv_variable cs_type_var_heap
	  cs & cs_type_var_heap=type_var_heap,cs_attr_var_heap=attr_var_heap
	= clear_atype_variables atype_vars cs
clear_atype_variables [] cs
	= cs

create_new_args_and_forward_old_args :: ![FreeVar] !*VarHeap -> (![FreeVar],!*VarHeap)
create_new_args_and_forward_old_args [fv=:{fv_info_ptr,fv_ident}:fvs] var_heap
	# (new_var_info_ptr,var_heap) = newPtr VI_Empty var_heap
	  var_heap = writePtr fv_info_ptr (VI_Variable fv_ident new_var_info_ptr) var_heap
	  fv & fv_info_ptr=new_var_info_ptr
	  (fvs,var_heap) = create_new_args_and_forward_old_args fvs var_heap
	= ([fv:fvs],var_heap)
create_new_args_and_forward_old_args [] var_heap
	= ([],var_heap)

replace_variables_determine_ref_counts_and_calls :: ![FreeVar] !Expression !*ExpressionHeap !*VarHeap
	-> (!FunctionBody,![FreeVar],![FunCall],![ExprInfoPtr],!*ExpressionHeap,!*VarHeap)
replace_variables_determine_ref_counts_and_calls args rhs expr_heap var_heap
	# var_heap = clearCount args cIsAGlobalVar var_heap
	  ces = {ces_var_heap=var_heap, ces_expression_heap=expr_heap, ces_free_vars=[], ces_dynamics=[], ces_fun_calls=[]}
	  (rhs,ces) = copy_expression rhs ces
	  {ces_free_vars=local_vars,ces_var_heap,ces_dynamics=dynamics,ces_expression_heap,ces_fun_calls} = ces
	  (args,var_heap) = retrieveRefCounts args ces_var_heap
	  (local_vars,var_heap) = retrieveRefCounts local_vars var_heap
	= (TransformedBody {tb_args=args,tb_rhs=rhs},local_vars,ces_fun_calls,dynamics,ces_expression_heap,var_heap)

class clearCount a :: !a !Bool !*VarHeap -> *VarHeap

instance clearCount [a] | clearCount a where
	clearCount [x:xs] locality var_heap
		= clearCount x locality (clearCount xs locality var_heap)
	clearCount [] locality var_heap
		= var_heap

instance clearCount LetBind where
	clearCount bind=:{lb_dst} locality var_heap
		= clearCount lb_dst locality var_heap

instance clearCount FreeVar where
	clearCount {fv_info_ptr} locality var_heap
		= var_heap <:= (fv_info_ptr, VI_Count 0 locality)

instance clearCount (FreeVar,a) where
	clearCount ({fv_info_ptr},_) locality var_heap
		= var_heap <:= (fv_info_ptr, VI_Count 0 locality)

retrieveRefCounts free_vars var_heap
	= mapSt retrieveRefCount free_vars var_heap

retrieveRefCount :: FreeVar *VarHeap -> (!FreeVar,!.VarHeap)
retrieveRefCount fv=:{fv_info_ptr} var_heap
	# (info, var_heap) = readPtr fv_info_ptr var_heap
	= case info of
		VI_Count count _
			-> ({fv & fv_count = count}, var_heap)
		VI_RefFromTupleSel0 count
			-> ({fv & fv_count = count}, var_heap)
		VI_RefFromArrayUpdate count _
			-> ({fv & fv_count = count}, var_heap)
		VI_RefFromArrayUpdateOfTupleElem2 count _
			-> ({fv & fv_count = count}, var_heap)
		VI_RefFromArrayUpdateToTupleSelector2 count _ _
			-> ({fv & fv_count = count}, var_heap)

::	VarInfo
	| VI_RefFromTupleSel0 !Int
	| VI_RefFromArrayUpdate !Int ![Selection]
	| VI_RefFromArrayUpdateToTupleSelector2 !Int ![Selection] !VarInfoPtr
	| VI_RefFromArrayUpdateOfTupleElem2 !Int ![Selection]

:: CopyExpressionState = {
	ces_var_heap :: !.VarHeap,
	ces_expression_heap :: !.ExpressionHeap,
	ces_free_vars :: ![FreeVar],
	ces_fun_calls	:: ![FunCall],
	ces_dynamics :: ![DynamicPtr]
   }

class copy_expression a :: !a !*CopyExpressionState -> (!a,!*CopyExpressionState)

instance copy_expression Expression where
	copy_expression (Var var) ces
		# (var, ces) = copy_expression var ces
		= (Var var, ces)
	copy_expression (App app=:{app_symb={symb_kind},app_args}) ces
		# (app_args, ces) = copy_expression app_args ces
		# ces = get_index symb_kind ces
		= (App {app & app_args = app_args}, ces)
	where
		get_index (SK_Function {glob_object,glob_module}) ces=:{ces_fun_calls}
			= {ces & ces_fun_calls = [FunCall glob_object 0 : ces_fun_calls]}
		get_index (SK_Constructor idx) ces
			= ces
		get_index (SK_LocalMacroFunction idx) ces=:{ces_fun_calls}
			= {ces & ces_fun_calls = [FunCall idx 0 : ces_fun_calls]}
		get_index (SK_OverloadedFunction _) ces
			= ces
	copy_expression (expr @ exprs) ces
		# ((expr, exprs), ces) = copy_expression (expr, exprs) ces
		= (expr @ exprs, ces)
	copy_expression (Let lad=:{let_strict_binds, let_lazy_binds, let_expr, let_info_ptr}) ces=:{ces_var_heap}
		# ces_var_heap = clear_counts let_strict_binds ces.ces_var_heap
		  ces_var_heap = clear_counts let_lazy_binds ces_var_heap
		  ces & ces_var_heap=ces_var_heap
		  (let_expr, ces) = copy_expression let_expr ces
		  (collected_strict_binds, collected_lazy_binds, ces)
			= collect_variables_in_binds let_strict_binds let_lazy_binds [] [] ces
		| collected_strict_binds=:[] && collected_lazy_binds=:[]
			= (let_expr, ces)
			# (let_strict_binds,ces_var_heap) = mark_unique_element_result_selectors collected_strict_binds ces.ces_var_heap
			  (let_lazy_binds,ces_var_heap) = mark_unique_element_result_selectors collected_lazy_binds ces_var_heap
			  ces & ces_var_heap=ces_var_heap
			= (Let {lad & let_expr = let_expr, let_strict_binds = let_strict_binds, let_lazy_binds = let_lazy_binds}, ces)
			with
				mark_unique_element_result_selectors :: ![LetBind] !*VarHeap -> (!*[LetBind],!*VarHeap)
				mark_unique_element_result_selectors [] var_heap
					= ([],var_heap)
				mark_unique_element_result_selectors [b=:{lb_dst={fv_info_ptr},lb_src=Selection UniqueSelector expr selections} : xs] var_heap
					| unique_result_selection selections fv_info_ptr var_heap
						# (lb,var_heap) = mark_unique_element_result_selectors xs var_heap
						  b & lb_src = Selection UniqueSelectorUniqueElementResult expr selections
						= ([b:lb],var_heap)
				mark_unique_element_result_selectors [b=:{lb_dst={fv_info_ptr},lb_src=Selection UniqueSingleArraySelector expr selections} : xs] var_heap
					| unique_result_selection selections fv_info_ptr var_heap
						# (lb,var_heap) = mark_unique_element_result_selectors xs var_heap
						  b & lb_src = Selection UniqueSingleArraySelectorUniqueElementResult expr selections
						= ([b:lb],var_heap)
				mark_unique_element_result_selectors [b:xs] var_heap
					# (lb,var_heap) = mark_unique_element_result_selectors xs var_heap
					= ([b:lb],var_heap)

				unique_result_selection selections fv_info_ptr var_heap
					= case sreadPtr fv_info_ptr var_heap of
						VI_RefFromArrayUpdateOfTupleElem2 _ update_selections
							-> same_selections selections update_selections
						_
							-> False
		where
			clear_counts [bind : binds] var_heap
				= clear_counts binds (clearCount bind cIsALocalVar var_heap)
			clear_counts [] var_heap
				= var_heap

			collect_variables_in_binds :: ![LetBind] ![LetBind] ![LetBind] ![LetBind] !*CopyExpressionState
															-> (![LetBind],![LetBind],!*CopyExpressionState)
			collect_variables_in_binds strict_binds lazy_binds collected_strict_binds collected_lazy_binds ces
				# (bind_fond, lazy_binds, collected_lazy_binds, ces)
					= examine_reachable_binds False lazy_binds collected_lazy_binds ces
				# (bind_fond, strict_binds, collected_strict_binds, ces)
					= examine_reachable_binds bind_fond strict_binds collected_strict_binds ces
				| bind_fond
					= collect_variables_in_binds strict_binds lazy_binds collected_strict_binds collected_lazy_binds ces
					= (collected_strict_binds, collected_lazy_binds, ces)

			examine_reachable_binds :: !Bool ![LetBind] ![LetBind] !*CopyExpressionState
								  -> *(!Bool,![LetBind],![LetBind],!*CopyExpressionState)
			examine_reachable_binds bind_found [letb=:{lb_dst=fv=:{fv_info_ptr},lb_src} : binds] collected_binds ces
				# (bind_found, binds, collected_binds, ces) = examine_reachable_binds bind_found binds collected_binds ces
				# (info, ces_var_heap) = readPtr fv_info_ptr ces.ces_var_heap
				# ces & ces_var_heap = ces_var_heap
				= case info of
					VI_Count count _
						| count > 0
							# (lb_src, ces) = copy_expression lb_src ces
							-> (True, binds, [{letb & lb_dst = {fv & fv_count = count}, lb_src = lb_src} : collected_binds], ces)
							-> (bind_found, [letb : binds], collected_binds, ces)
					VI_RefFromTupleSel0 count
						# (lb_src, ces) = copy_expression lb_src ces
						-> (True, binds, [{letb & lb_dst = {fv & fv_count = count}, lb_src = lb_src} : collected_binds], ces)
					VI_RefFromArrayUpdate count selectors
						-> case lb_src of
							TupleSelect tuple_symbol 1 (Var var)
								# (var, ces) = copyUpdateVarTupleSelect2Var var fv_info_ptr count selectors ces
								# lb_src = TupleSelect tuple_symbol 1 (Var var)
								-> (True, binds, [{letb & lb_dst = {fv & fv_count = count}, lb_src = lb_src} : collected_binds], ces)
							_
								#  (lb_src, ces) = copy_expression lb_src ces
								-> (True, binds, [{letb & lb_dst = {fv & fv_count = count}, lb_src = lb_src} : collected_binds], ces)
					VI_RefFromArrayUpdateOfTupleElem2 count _
						#  (lb_src, ces) = copy_expression lb_src ces
						-> (True, binds, [{letb & lb_dst = {fv & fv_count = count}, lb_src = lb_src} : collected_binds], ces)
					VI_RefFromArrayUpdateToTupleSelector2 count selectors array_var_info_ptr
						-> abort "examine_reachable_binds VI_RefFromArrayUpdateToTupleSelector2"
			examine_reachable_binds bind_found [] collected_binds ces
				= (bind_found, [], collected_binds, ces)
	copy_expression (Case case_expr) ces
		# (case_expr, ces) = copy_expression case_expr ces
		= (Case case_expr, ces)
	copy_expression (Selection is_unique expr selectors) ces
		# ((expr, selectors), ces) = copy_expression (expr, selectors) ces
		= (Selection is_unique expr selectors, ces)
	copy_expression (Update (Var var) selectors expr2) ces
		# (var, ces) = copyUpdateVar var selectors ces
		# ((expr2, selectors), ces) = copy_expression (expr2, selectors) ces
		= (Update (Var var) selectors expr2, ces)
	copy_expression (Update (TupleSelect tuple_symbol 1 (Var var)) selectors expr2) ces
		# (var, ces) = copyUpdateTupleSelect2Var var selectors ces
		# ((expr2, selectors), ces) = copy_expression (expr2, selectors) ces
		= (Update (TupleSelect tuple_symbol 1 (Var var)) selectors expr2, ces)
	copy_expression (Update expr1 selectors expr2) ces
		# (((expr1, expr2), selectors), ces) = copy_expression ((expr1, expr2), selectors) ces
		= (Update expr1 selectors expr2, ces)
	copy_expression (RecordUpdate cons_symbol expression expressions) ces
		# ((expression, expressions), ces) = copy_expression (expression, expressions) ces
		= (RecordUpdate cons_symbol expression expressions, ces)
	copy_expression (TupleSelect symbol 0 (Var var)) ces
		# (var, ces) = collectTupleSelect0Var var ces
		= (TupleSelect symbol 0 (Var var), ces)
	copy_expression (TupleSelect symbol argn_nr expr) ces
		# (expr, ces) = copy_expression expr ces
		= (TupleSelect symbol argn_nr expr, ces)
	copy_expression (MatchExpr cons_ident expr) ces
		# (expr, ces) = copy_expression expr ces
		= (MatchExpr cons_ident expr, ces)
	copy_expression (IsConstructor expr cons_symbol cons_arity global_type_index case_ident position) ces
		# (expr, ces) = copy_expression expr ces
		= (IsConstructor expr cons_symbol cons_arity global_type_index case_ident position, ces)
	copy_expression (DynamicExpr dynamic_expr) ces
		# (dynamic_expr, ces) = copy_expression dynamic_expr ces
		= (DynamicExpr dynamic_expr, ces)
	copy_expression (TypeSignature type_function expr) ces
		# (expr, ces) = copy_expression expr ces
		= (TypeSignature type_function expr, ces)
	copy_expression (DictionariesFunction dictionaries expr expr_type) ces
		# ces = {ces & ces_var_heap = clearCount dictionaries cIsALocalVar ces.ces_var_heap}
		  (expr, ces) = copy_expression expr ces
		  (dictionaries, var_heap) = mapSt retrieve_ref_count dictionaries ces.ces_var_heap
		  ces = {ces & ces_var_heap = var_heap}
		= (DictionariesFunction dictionaries expr expr_type, ces)
	where
		retrieve_ref_count (fv,a_type) var_heap
			# (fv,var_heap) = retrieveRefCount fv var_heap
			= ((fv,a_type),var_heap)
	copy_expression expr ces
		= (expr, ces)

instance copy_expression Selection where
	copy_expression record_selection=:(RecordSelection _ _) ces
		= (record_selection, ces)
	copy_expression (ArraySelection array_select expr_ptr index_expr) ces
		# (index_expr, ces) = copy_expression index_expr ces
		= (ArraySelection array_select expr_ptr index_expr, ces)
	copy_expression (SafeArraySelection array_select expr_ptr index_expr) ces
		# (index_expr, ces) = copy_expression index_expr ces
		= (SafeArraySelection array_select expr_ptr index_expr, ces)
	copy_expression (DictionarySelection dictionary_select selectors expr_ptr index_expr) ces
		# ((index_expr,selectors), ces) = copy_expression (index_expr,selectors) ces
		= (DictionarySelection dictionary_select selectors expr_ptr index_expr, ces)
	copy_expression (SafeDictionarySelection dictionary_select selectors expr_ptr index_expr) ces
		# ((index_expr,selectors), ces) = copy_expression (index_expr,selectors) ces
		= (SafeDictionarySelection dictionary_select selectors expr_ptr index_expr, ces)

instance copy_expression [a] | copy_expression a where
	copy_expression [x:xs] ces
		# (x, ces) = copy_expression x ces
		# (xs, ces) = copy_expression xs ces
		= ([x:xs], ces)
	copy_expression [] ces
		= ([], ces)

instance copy_expression (!a,!b) | copy_expression a & copy_expression b where
	copy_expression (x,y) ces
		# (x, ces) = copy_expression x ces
		# (y, ces) = copy_expression y ces
		= ((x,y), ces)

instance copy_expression (Optional a) | copy_expression a where
	copy_expression (Yes x) ces
		# (x, ces) = copy_expression x ces
		= (Yes x, ces)
	copy_expression no ces
		= (no, ces)

instance copy_expression (Bind a b) | copy_expression a where
	copy_expression bind=:{bind_src} ces
		# (bind_src, ces) = copy_expression bind_src ces
		= ({bind & bind_src = bind_src}, ces)

instance copy_expression Case where
	copy_expression kees=:{ case_expr, case_guards, case_default } ces
		# (case_expr, ces) = copy_expression case_expr ces
		# (case_guards, ces) = copy_expression case_guards ces
		# (case_default, ces) = copy_expression case_default ces
		=  ({kees & case_expr = case_expr, case_guards = case_guards, case_default = case_default}, ces)

instance copy_expression CasePatterns where
	copy_expression (AlgebraicPatterns type patterns) ces
		# (patterns, ces) = copy_expression patterns ces
		= (AlgebraicPatterns type patterns, ces)
	copy_expression (BasicPatterns type patterns) ces
		# (patterns, ces) = copy_expression patterns ces
		= (BasicPatterns type patterns, ces)
	copy_expression (OverloadedPatterns type decons_expr patterns) ces
		# (patterns, ces) = copy_expression patterns ces
		= (OverloadedPatterns type decons_expr patterns, ces)
	copy_expression (NewTypePatterns type patterns) ces
		# (patterns, ces) = copy_expression patterns ces
		= (NewTypePatterns type patterns, ces)
	copy_expression (DynamicPatterns patterns) ces
		# (patterns, ces) = copy_expression patterns ces
		= (DynamicPatterns patterns, ces)
	copy_expression NoPattern ces
		= (NoPattern, ces)

instance copy_expression AlgebraicPattern where
	copy_expression pattern=:{ap_vars,ap_expr} ces
		# ces = {ces & ces_var_heap = clearCount ap_vars cIsALocalVar ces.ces_var_heap}
		  (ap_expr, ces) = copy_expression ap_expr ces
		  (ap_vars, ces_var_heap) = retrieveRefCounts ap_vars ces.ces_var_heap
		= ({pattern & ap_expr = ap_expr, ap_vars = ap_vars}, {ces & ces_var_heap = ces_var_heap})

instance copy_expression BasicPattern where
	copy_expression pattern=:{bp_expr} ces
		# (bp_expr, ces) = copy_expression bp_expr ces
		= ({pattern & bp_expr = bp_expr}, ces)

instance copy_expression DynamicPattern where
	copy_expression pattern=:{dp_var,dp_rhs,dp_type} ces=:{ces_dynamics,ces_var_heap,ces_expression_heap}
		# ces_var_heap = clearCount dp_var cIsALocalVar ces_var_heap
		  (EI_DynamicTypeWithVars vars type _, ces_expression_heap) = readPtr dp_type ces_expression_heap
		  ces & ces_var_heap = ces_var_heap, ces_expression_heap = ces_expression_heap
		  (dp_rhs, ces) = copy_expression dp_rhs {ces & ces_dynamics=[]}
		  local_dynamics = ces.ces_dynamics
		  ces_expression_heap = ces.ces_expression_heap <:= (dp_type, EI_DynamicTypeWithVars vars type local_dynamics)
		  (dp_var, ces_var_heap) = retrieveRefCount dp_var ces.ces_var_heap
		  ces_dynamics = [dp_type:ces_dynamics]
		  ces & ces_dynamics=ces_dynamics, ces_var_heap=ces_var_heap, ces_expression_heap=ces_expression_heap
		= ({pattern & dp_rhs = dp_rhs, dp_var = dp_var}, ces)

instance copy_expression DynamicExpr where
	copy_expression dynamic_expr=:{dyn_expr, dyn_info_ptr} ces=:{ces_dynamics}
		# (dyn_expr, ces=:{ces_expression_heap}) = copy_expression dyn_expr {ces & ces_dynamics=[]}
		  local_dynamics = ces.ces_dynamics
		  ces_expression_heap = mark_used_dynamic dyn_info_ptr local_dynamics (readPtr dyn_info_ptr ces_expression_heap)
		  ces_dynamics = [dyn_info_ptr:ces_dynamics]
		= ({dynamic_expr & dyn_expr=dyn_expr}, {ces & ces_dynamics=ces_dynamics, ces_expression_heap=ces_expression_heap})
	where
		mark_used_dynamic dyn_info_ptr local_dynamics (EI_UnmarkedDynamic opt_type _, symbol_heap)
			= symbol_heap <:= (dyn_info_ptr, EI_Dynamic opt_type local_dynamics)
		mark_used_dynamic dyn_info_ptr local_dynamics (EI_Dynamic opt_type _, symbol_heap)
			= symbol_heap <:= (dyn_info_ptr, EI_Dynamic opt_type local_dynamics)

instance copy_expression BoundVar where
	copy_expression var=:{var_ident,var_info_ptr,var_expr_ptr} ces=:{ces_var_heap}
		# (var_info, ces_var_heap) = readPtr var_info_ptr ces_var_heap
		  ces & ces_var_heap = ces_var_heap
		= case var_info of
			VI_Count count is_global
				| count > 0 || is_global
					-> (var, {ces & ces_var_heap = writePtr var_info_ptr (VI_Count (inc count) is_global) ces.ces_var_heap})
					# ces_free_vars = [{fv_ident = var_ident, fv_info_ptr = var_info_ptr, fv_def_level = NotALevel, fv_count = 0} : ces.ces_free_vars]
					# ces_var_heap = writePtr var_info_ptr (VI_Count 1 is_global) ces.ces_var_heap
					-> (var, {ces & ces_free_vars=ces_free_vars, ces_var_heap=ces_var_heap})
			VI_Variable var_ident var_info_ptr
				# (var_expr_ptr,expression_heap) = newPtr EI_Empty ces.ces_expression_heap
				  ces & ces_expression_heap=expression_heap
				  var = {var_ident=var_ident, var_info_ptr=var_info_ptr, var_expr_ptr=var_expr_ptr}
				-> copy_expression var ces
			VI_RefFromTupleSel0 count
				-> (var, {ces & ces_var_heap = writePtr var_info_ptr (VI_Count (inc count) False) ces.ces_var_heap})
			VI_RefFromArrayUpdate count _
				-> (var, {ces & ces_var_heap = writePtr var_info_ptr (VI_Count (inc count) False) ces.ces_var_heap})
			VI_RefFromArrayUpdateToTupleSelector2 count _ array_var_info_ptr
				# ces_var_heap = remove_VI_RefFromArrayUpdateOfTupleElem2 array_var_info_ptr ces_var_heap
				# ces_var_heap = writePtr var_info_ptr (VI_Count (inc count) False) ces.ces_var_heap
				-> (var, {ces & ces_var_heap = ces_var_heap})
			VI_RefFromArrayUpdateOfTupleElem2 count _
				-> (var, {ces & ces_var_heap = writePtr var_info_ptr (VI_Count (inc count) False) ces.ces_var_heap})
			_
				-> abort "copy_expression [BoundVar] (type_gadt)"

collectTupleSelect0Var :: !BoundVar !*CopyExpressionState -> (!BoundVar,!*CopyExpressionState)
collectTupleSelect0Var var=:{var_ident,var_info_ptr,var_expr_ptr} ces=:{ces_var_heap}
	# (var_info, ces_var_heap) = readPtr var_info_ptr ces_var_heap
	  ces & ces_var_heap = ces_var_heap
	= case var_info of
		VI_Count count is_global
			| count > 0 || is_global
				-> (var, {ces & ces_var_heap = writePtr var_info_ptr (VI_Count (inc count) is_global) ces.ces_var_heap})
				# ces_free_vars = [{fv_ident = var_ident, fv_info_ptr = var_info_ptr, fv_def_level = NotALevel, fv_count = 0} : ces.ces_free_vars]
				# ces_var_heap = writePtr var_info_ptr (VI_RefFromTupleSel0 1) ces.ces_var_heap
				-> (var, {ces & ces_free_vars=ces_free_vars, ces_var_heap=ces_var_heap})
		VI_Variable var_ident var_info_ptr
			# (var_expr_ptr,expression_heap) = newPtr EI_Empty ces.ces_expression_heap
			  ces & ces_expression_heap=expression_heap
			  var = {var_ident=var_ident, var_info_ptr=var_info_ptr, var_expr_ptr=var_expr_ptr}
			-> collectTupleSelect0Var var ces
		VI_RefFromTupleSel0 count
			-> (var, {ces & ces_var_heap = writePtr var_info_ptr (VI_RefFromTupleSel0 (inc count)) ces.ces_var_heap})
		VI_RefFromArrayUpdate count _
			-> (var, {ces & ces_var_heap = writePtr var_info_ptr (VI_Count (inc count) False) ces.ces_var_heap})
		VI_RefFromArrayUpdateToTupleSelector2 count _ array_var_info_ptr
			# ces_var_heap = remove_VI_RefFromArrayUpdateOfTupleElem2 array_var_info_ptr ces_var_heap
			# ces_var_heap = writePtr var_info_ptr (VI_Count (inc count) False) ces.ces_var_heap
			-> (var, {ces & ces_var_heap = ces_var_heap})
		VI_RefFromArrayUpdateOfTupleElem2 count selectors
			-> (var, {ces & ces_var_heap = writePtr var_info_ptr (VI_RefFromArrayUpdateOfTupleElem2 (inc count) selectors) ces.ces_var_heap})

remove_VI_RefFromArrayUpdateOfTupleElem2 array_var_info_ptr var_heap
	# (array_var_info, var_heap) = readPtr array_var_info_ptr var_heap
	= case array_var_info of
		VI_RefFromArrayUpdateOfTupleElem2 count _
			-> writePtr array_var_info_ptr (VI_Count count False) var_heap
		_
			-> var_heap

copyUpdateVar :: !BoundVar ![Selection] !*CopyExpressionState -> (!BoundVar,!*CopyExpressionState)
copyUpdateVar var=:{var_ident,var_info_ptr,var_expr_ptr} update_selectors ces=:{ces_var_heap}
	# (var_info, ces_var_heap) = readPtr var_info_ptr ces_var_heap
	= case var_info of
		VI_Count count is_global
			| count > 0 || is_global
				-> (var, {ces & ces_var_heap = writePtr var_info_ptr (VI_Count (inc count) is_global) ces_var_heap})
				# ces_free_vars = [{fv_ident = var_ident, fv_info_ptr = var_info_ptr, fv_def_level = NotALevel, fv_count = 0} : ces.ces_free_vars]
				# ces_var_heap = writePtr var_info_ptr (VI_RefFromArrayUpdate 1 update_selectors) ces_var_heap
				-> (var, {ces & ces_free_vars=ces_free_vars, ces_var_heap=ces_var_heap})
		VI_Variable var_ident var_info_ptr
			# (var_expr_ptr,expression_heap) = newPtr EI_Empty ces.ces_expression_heap
			  ces & ces_var_heap=ces_var_heap, ces_expression_heap=expression_heap
			  var = {var_ident=var_ident, var_info_ptr=var_info_ptr, var_expr_ptr=var_expr_ptr}
			-> copyUpdateVar var update_selectors ces
		VI_RefFromTupleSel0 count
			-> (var, {ces & ces_var_heap = writePtr var_info_ptr (VI_Count (inc count) False) ces_var_heap})
		VI_RefFromArrayUpdate count selectors
			| same_selections selectors update_selectors
				-> (var, {ces & ces_var_heap = writePtr var_info_ptr (VI_RefFromArrayUpdate (inc count) update_selectors) ces_var_heap})
				-> (var, {ces & ces_var_heap = writePtr var_info_ptr (VI_Count (inc count) False) ces_var_heap})
		VI_RefFromArrayUpdateToTupleSelector2 count selectors array_var_info_ptr
			# ces_var_heap = remove_VI_RefFromArrayUpdateOfTupleElem2 array_var_info_ptr ces_var_heap
			# ces_var_heap = writePtr var_info_ptr (VI_Count (inc count) False) ces_var_heap
			-> (var, {ces & ces_var_heap = ces_var_heap})
		VI_RefFromArrayUpdateOfTupleElem2 count _
			-> (var, {ces & ces_var_heap = writePtr var_info_ptr (VI_Count (inc count) False) ces_var_heap})

copyUpdateTupleSelect2Var :: !BoundVar ![Selection] !*CopyExpressionState -> (!BoundVar,!*CopyExpressionState)
copyUpdateTupleSelect2Var var=:{var_ident,var_info_ptr,var_expr_ptr} update_selectors ces=:{ces_var_heap}
	# (var_info, ces_var_heap) = readPtr var_info_ptr ces_var_heap
	= case var_info of
		VI_Count count is_global
			| count > 0 || is_global
				-> (var, {ces & ces_var_heap = writePtr var_info_ptr (VI_Count (inc count) is_global) ces_var_heap})
				# ces_free_vars = [{fv_ident = var_ident, fv_info_ptr = var_info_ptr, fv_def_level = NotALevel, fv_count = 0} : ces.ces_free_vars]
				# ces_var_heap = writePtr var_info_ptr (VI_RefFromArrayUpdateOfTupleElem2 1 update_selectors) ces_var_heap
				-> (var, {ces & ces_free_vars=ces_free_vars, ces_var_heap=ces_var_heap})
		VI_Variable var_ident var_info_ptr
			# (var_expr_ptr,expression_heap) = newPtr EI_Empty ces.ces_expression_heap
			  ces & ces_var_heap=ces_var_heap, ces_expression_heap=expression_heap
			  var = {var_ident=var_ident, var_info_ptr=var_info_ptr, var_expr_ptr=var_expr_ptr}
			-> copyUpdateTupleSelect2Var var update_selectors ces
		VI_RefFromTupleSel0 count
			-> (var, {ces & ces_var_heap = writePtr var_info_ptr (VI_RefFromArrayUpdateOfTupleElem2 (inc count) update_selectors) ces_var_heap})
		VI_RefFromArrayUpdate count _
			-> (var, {ces & ces_var_heap = writePtr var_info_ptr (VI_Count (inc count) False) ces_var_heap})
		VI_RefFromArrayUpdateToTupleSelector2 count selectors array_var_info_ptr
			# ces_var_heap = remove_VI_RefFromArrayUpdateOfTupleElem2 array_var_info_ptr ces_var_heap
			# ces_var_heap = writePtr var_info_ptr (VI_Count (inc count) False) ces_var_heap
			-> (var, {ces & ces_var_heap = ces_var_heap})
		VI_RefFromArrayUpdateOfTupleElem2 count selectors
			| same_selections selectors update_selectors
				-> (var, {ces & ces_var_heap = writePtr var_info_ptr (VI_RefFromArrayUpdateOfTupleElem2 (inc count) update_selectors) ces_var_heap})
				-> (var, {ces & ces_var_heap = writePtr var_info_ptr (VI_Count (inc count) False) ces_var_heap})

copyUpdateVarTupleSelect2Var :: !BoundVar !VarInfoPtr !Int ![Selection] !*CopyExpressionState -> (!BoundVar,!*CopyExpressionState)
copyUpdateVarTupleSelect2Var var=:{var_ident,var_info_ptr,var_expr_ptr} array_var_info_ptr count update_selectors ces=:{ces_var_heap}
	# (var_info, ces_var_heap) = readPtr var_info_ptr ces_var_heap
	= case var_info of
		VI_Count count is_global
			| count > 0 || is_global
				-> (var, {ces & ces_var_heap = writePtr var_info_ptr (VI_Count (inc count) is_global) ces_var_heap})
				# ces_var_heap = writePtr var_info_ptr (VI_RefFromArrayUpdateOfTupleElem2 1 update_selectors) ces_var_heap
				# ces_var_heap = writePtr array_var_info_ptr (VI_RefFromArrayUpdateToTupleSelector2 count update_selectors var_info_ptr) ces_var_heap
				# ces_free_vars = [{fv_ident = var_ident, fv_info_ptr = var_info_ptr, fv_def_level = NotALevel, fv_count = 0} : ces.ces_free_vars]
				-> (var, {ces & ces_free_vars=ces_free_vars, ces_var_heap=ces_var_heap})
		VI_Variable var_ident var_info_ptr
			# (var_expr_ptr,expression_heap) = newPtr EI_Empty ces.ces_expression_heap
			  ces & ces_var_heap=ces_var_heap, ces_expression_heap=expression_heap
			  var = {var_ident=var_ident, var_info_ptr=var_info_ptr, var_expr_ptr=var_expr_ptr}
			-> copyUpdateVarTupleSelect2Var var array_var_info_ptr count update_selectors ces
		VI_RefFromTupleSel0 count
			# ces_var_heap = writePtr var_info_ptr (VI_RefFromArrayUpdateOfTupleElem2 (inc count) update_selectors) ces_var_heap
			# ces_var_heap = writePtr array_var_info_ptr (VI_RefFromArrayUpdateToTupleSelector2 count update_selectors var_info_ptr) ces_var_heap
			-> (var, {ces & ces_var_heap = ces_var_heap})
		VI_RefFromArrayUpdate count _
			-> (var, {ces & ces_var_heap = writePtr var_info_ptr (VI_Count (inc count) False) ces_var_heap})
		VI_RefFromArrayUpdateToTupleSelector2 count selectors array_var_info_ptr
			# ces_var_heap = remove_VI_RefFromArrayUpdateOfTupleElem2 array_var_info_ptr ces_var_heap
			# ces_var_heap = writePtr var_info_ptr (VI_Count (inc count) False) ces_var_heap
			-> (var, {ces & ces_var_heap = ces_var_heap})
		VI_RefFromArrayUpdateOfTupleElem2 count selectors
			| same_selections selectors update_selectors
				# ces_var_heap = writePtr var_info_ptr (VI_RefFromArrayUpdateOfTupleElem2 (inc count) update_selectors) ces_var_heap
				# ces_var_heap = writePtr array_var_info_ptr (VI_RefFromArrayUpdateToTupleSelector2 count update_selectors var_info_ptr) ces_var_heap
				-> (var, {ces & ces_var_heap = ces_var_heap})
				-> (var, {ces & ces_var_heap = writePtr var_info_ptr (VI_Count (inc count) False) ces_var_heap})

same_selections [RecordSelection {glob_module=m1,glob_object={ds_index=i1}} f1:selections1] [RecordSelection {glob_module=m2,glob_object={ds_index=i2}} f2:selections2]
	= f1==f2 && m1==m2 && i1==i2 && same_selections selections1 selections2
same_selections [ArraySelection array_select1 _ index_expr1:selections1] [ArraySelection array_select2 _ index_expr2:selections2]
	= equal_index index_expr1 index_expr2 && same_selections selections1 selections2
same_selections [SafeArraySelection array_select1 _ index_expr1:selections1] [SafeArraySelection array_select2 _ index_expr2:selections2]
	= equal_index index_expr1 index_expr2 && same_selections selections1 selections2
same_selections [] []
	= True
same_selections selections update_selections
	= False

equal_index (Var {var_info_ptr=var_info_ptr1}) (Var {var_info_ptr=var_info_ptr2})
	= var_info_ptr1==var_info_ptr2
equal_index (BasicExpr (BVInt i1)) (BasicExpr (BVInt i2))
	= i1==i2
equal_index _ _
	= False

::	TypeVarInfo
	| TVI_SubstSpecifiedForGADType !Type	// from GADT type to specified type or fresh GADT type
	| TVI_SubstSpecifiedType !Type			// from and to specified type or fresh GADT type

simplifyAndCheckTypeApplication :: !Type ![AType] -> (!Bool, !Type)
simplifyAndCheckTypeApplication (TA type_cons=:{type_arity} cons_args) type_args
	= (True, TA { type_cons & type_arity = type_arity + length type_args } (cons_args ++ type_args))
simplifyAndCheckTypeApplication (TAS type_cons=:{type_arity} cons_args strictness) type_args
	= (True, TAS { type_cons & type_arity = type_arity + length type_args } (cons_args ++ type_args) strictness)
simplifyAndCheckTypeApplication (cons_var :@: types) type_args
	= (True, cons_var :@: (types ++ type_args))
simplifyAndCheckTypeApplication (TV tv) type_args
	= (True, CV tv :@: type_args)
simplifyAndCheckTypeApplication TArrow [type1, type2]
	= (True, type1 --> type2)
simplifyAndCheckTypeApplication TArrow [type]
	= (True, TArrow1 type)
simplifyAndCheckTypeApplication (TArrow1 type1) [type2]
	= (True, type1 --> type2)
simplifyAndCheckTypeApplication type type_args
	= (False, type)

is_gadt_case :: Expression {#CommonDefs} -> Bool
is_gadt_case
		(Case {case_expr=Var _,case_guards=AlgebraicPatterns {gi_module,gi_index} patterns,case_default=No,case_explicit=False}) common_defs
	| common_defs.[gi_module].com_type_defs.[gi_index].td_rhs =: GeneralisedAlgType _
		= has_gadt_cons patterns common_defs
		= False
where
	has_gadt_cons [{ap_symbol={glob_object,glob_module}}:patterns] common_defs
		= common_defs.[glob_module].com_cons_defs.[glob_object.ds_index].cons_gadt_type =: Yes _ || has_gadt_cons patterns common_defs
	has_gadt_cons [] common_defs
		= False
is_gadt_case _ _
	= False

get_var_info_ptr_argument_n :: ![FreeVar] !Int !VarInfoPtr -> Int
get_var_info_ptr_argument_n [{fv_info_ptr}:args] var_n var_info_ptr
	| fv_info_ptr==var_info_ptr
		= var_n
		= get_var_info_ptr_argument_n args (var_n+1) var_info_ptr
get_var_info_ptr_argument_n [] var_n var_info_ptr
	= -1

get_gadt_case_argument_n tb_args (Case {case_expr=Var {var_info_ptr}})
	= get_var_info_ptr_argument_n tb_args 0 var_info_ptr

:: EqualTypeVarInfoPtr = { tv_info_ptr1 :: !TypeVarInfoPtr, tv_info_ptr2 :: !TypeVarInfoPtr }

equal_gadt_type_and_vars :: GADTResult GADTResult -> Bool
equal_gadt_type_and_vars {gadt_type={at_type=gadt_type1}} {gadt_type={at_type=gadt_type2}}
	# (gadt_types_equal,equal_type_var_info_ptrs) = equal_gadt_type gadt_type1 gadt_type2 [!]
	= gadt_types_equal
where
	equal_gadt_type :: !Type !Type ![!EqualTypeVarInfoPtr] -> (!Bool,![!EqualTypeVarInfoPtr])
	equal_gadt_type (TB b1) (TB b2) equal_type_var_info_ptrs
		= (b1==b2, equal_type_var_info_ptrs)
	equal_gadt_type (TA {type_index=ti1} atypes1) (TA {type_index=ti2} atypes2) equal_type_var_info_ptrs
		| ti1.glob_object==ti2.glob_object && ti1.glob_module==ti2.glob_module
			= equal_gadt_atypes atypes1 atypes2 equal_type_var_info_ptrs
			= (False, equal_type_var_info_ptrs)
	equal_gadt_type (TAS {type_index=ti1} atypes1 _) (TAS {type_index=ti2} atypes2 _) equal_type_var_info_ptrs
		| ti1.glob_object==ti2.glob_object && ti1.glob_module==ti2.glob_module
			= equal_gadt_atypes atypes1 atypes2 equal_type_var_info_ptrs
			= (False, equal_type_var_info_ptrs)
	equal_gadt_type (TV tv1) (TV tv2) equal_type_var_info_ptrs
		= equal_gadt_tv {tv_info_ptr1=tv1.tv_info_ptr,tv_info_ptr2=tv2.tv_info_ptr} equal_type_var_info_ptrs
	equal_gadt_type (a1 --> b1) (a2 --> b2) equal_type_var_info_ptrs
		# (gadt_types_equal, equal_type_var_info_ptrs) = equal_gadt_atype a1 a2 equal_type_var_info_ptrs
		| gadt_types_equal
			= equal_gadt_atype b1 b2 equal_type_var_info_ptrs
			= (False, equal_type_var_info_ptrs)
	equal_gadt_type (CV cv1 :@: atypes1) (CV cv2 :@: atypes2) equal_type_var_info_ptrs
		# (gadt_tvs_equal, equal_type_var_info_ptrs)
			= equal_gadt_tv {tv_info_ptr1=cv1.tv_info_ptr,tv_info_ptr2=cv2.tv_info_ptr} equal_type_var_info_ptrs
		| gadt_tvs_equal
			= equal_gadt_atypes atypes1 atypes2 equal_type_var_info_ptrs
			= (False, equal_type_var_info_ptrs)
	equal_gadt_type (TArrow1 a1) (TArrow1 a2) equal_type_var_info_ptrs
		= equal_gadt_atype a1 a2 equal_type_var_info_ptrs
	equal_gadt_type TArrow TArrow equal_type_var_info_ptrs
		= (True, equal_type_var_info_ptrs)
	equal_gadt_type _ _ equal_type_var_info_ptrs
		= (False, equal_type_var_info_ptrs)

	equal_gadt_atype :: !AType !AType ![!EqualTypeVarInfoPtr] -> (!Bool,![!EqualTypeVarInfoPtr])
	equal_gadt_atype {at_attribute=attribute1,at_type=type1} {at_attribute=attribute2,at_type=type2} equal_type_var_info_ptrs
		| attribute1==attribute2
			= equal_gadt_type type1 type2 equal_type_var_info_ptrs
			= (False, equal_type_var_info_ptrs)

	equal_gadt_atypes :: ![AType] ![AType] ![!EqualTypeVarInfoPtr] -> (!Bool,![!EqualTypeVarInfoPtr])
	equal_gadt_atypes [{at_attribute=attribute1,at_type=type1}:atypes1] [{at_attribute=attribute2,at_type=type2}:atypes2] equal_type_var_info_ptrs
		| attribute1==attribute2
			# (gadt_types_equal, equal_type_var_info_ptrs) = equal_gadt_type type1 type2 equal_type_var_info_ptrs
			| gadt_types_equal
				= equal_gadt_atypes atypes1 atypes2 equal_type_var_info_ptrs
				= (False, equal_type_var_info_ptrs)
			= (False, equal_type_var_info_ptrs)
	equal_gadt_atypes [] [] equal_type_var_info_ptrs
		= (True, equal_type_var_info_ptrs)
	equal_gadt_atypes _ _ equal_type_var_info_ptrs
		= (False, equal_type_var_info_ptrs)

	equal_gadt_tv :: !EqualTypeVarInfoPtr ![!EqualTypeVarInfoPtr] -> (!Bool,![!EqualTypeVarInfoPtr])
	equal_gadt_tv equal_type_var_info_ptr equal_type_var_info_ptrs
		# (occurs,equal) = equal_previous_type_var_info_ptr equal_type_var_info_ptr equal_type_var_info_ptrs
		| occurs
			= (equal, equal_type_var_info_ptrs)
			= (True,[!equal_type_var_info_ptr:equal_type_var_info_ptrs])

	equal_previous_type_var_info_ptr :: !EqualTypeVarInfoPtr ![!EqualTypeVarInfoPtr] -> (!Bool,!Bool)
	equal_previous_type_var_info_ptr x [!hd:tl]
		| hd.tv_info_ptr1==x.tv_info_ptr1
			| hd.tv_info_ptr2==x.tv_info_ptr2
				= (True,True)
				= (True,False)
		| hd.tv_info_ptr2==x.tv_info_ptr2
			= (True,False)
			= equal_previous_type_var_info_ptr x tl
	equal_previous_type_var_info_ptr x [!]
		= (False,False)

get_gadt_case_type_and_vars :: ![AlgebraicPattern] !(Optional GADTResult) !{#CommonDefs} -> Optional GADTResult
get_gadt_case_type_and_vars [{ap_symbol={glob_object,glob_module}}:patterns] previous_gadt_type common_defs
	# {cons_gadt_type} = common_defs.[glob_module].com_cons_defs.[glob_object.ds_index]
	= case cons_gadt_type of
		No
			-> No
		Yes gadt_type_and_vars
			-> case previous_gadt_type of
				No
					-> get_gadt_case_type_and_vars patterns cons_gadt_type common_defs
				Yes previous_gadt_type_and_vars
					| equal_gadt_type_and_vars gadt_type_and_vars previous_gadt_type_and_vars
						-> get_gadt_case_type_and_vars patterns previous_gadt_type common_defs
						-> No
get_gadt_case_type_and_vars [] previous_gadt_type common_defs
	= previous_gadt_type

get_fun_body_gadt_case_type_and_vars_for_function_with_gadt_case :: !Expression !{#CommonDefs} -> Optional GADTResult
get_fun_body_gadt_case_type_and_vars_for_function_with_gadt_case tb_rhs common_defs
	| is_gadt_case tb_rhs common_defs
		# (Case {case_guards=AlgebraicPatterns _ patterns}) = tb_rhs
		= get_gadt_case_type_and_vars patterns No common_defs
		= No

get_more_gadt_cases :: !Int !VarInfoPtr ![FreeVar] !Expression !{#CommonDefs} -> [(VarInfoPtr,GADTResult)]
get_more_gadt_cases case_depth case_var_info_ptr tb_args (Case {case_guards=AlgebraicPatterns _ patterns}) common_defs
	= get_more_gadt_cases case_depth [case_var_info_ptr] tb_args patterns common_defs
where
	get_more_gadt_cases case_depth case_var_info_ptrs tb_args patterns common_defs
		= case get_more_gadt_cases_in_patterns case_depth patterns No case_var_info_ptrs tb_args common_defs of
			No
				-> []
			Yes var_info_ptr_and_gadt_result=:(next_case_var_info_ptr,_)
				# more_gadt_cases = get_more_gadt_cases (case_depth+1) [next_case_var_info_ptr:case_var_info_ptrs] tb_args patterns common_defs
				-> [var_info_ptr_and_gadt_result:more_gadt_cases]

	get_more_gadt_cases_in_patterns :: Int [AlgebraicPattern] (Optional (VarInfoPtr,GADTResult)) [VarInfoPtr] [FreeVar] {#CommonDefs}
															-> Optional (VarInfoPtr,GADTResult)
	get_more_gadt_cases_in_patterns case_depth [{ap_expr}:patterns] opt_next_gadt_case case_var_info_ptrs tb_args common_defs
		| case_depth>1
			# (Case {case_guards=AlgebraicPatterns _ next_case_patterns}) = ap_expr
			# opt_next_gadt_case = get_more_gadt_cases_in_patterns (case_depth-1) next_case_patterns opt_next_gadt_case case_var_info_ptrs tb_args common_defs
			= get_more_gadt_cases_in_patterns case_depth patterns opt_next_gadt_case case_var_info_ptrs tb_args common_defs
			# opt_next_gadt_case = get_next_gadt_case_in_next_case ap_expr opt_next_gadt_case case_var_info_ptrs tb_args common_defs
			| opt_next_gadt_case=:No
				= No
			= get_more_gadt_cases_in_patterns case_depth patterns opt_next_gadt_case case_var_info_ptrs tb_args common_defs
	get_more_gadt_cases_in_patterns case_depth [] opt_next_gadt_case case_var_info_ptrs tb_args common_defs
		= opt_next_gadt_case

	get_next_gadt_case_in_next_case :: Expression (Optional (VarInfoPtr,GADTResult)) [VarInfoPtr] [FreeVar] {#CommonDefs}
												-> Optional (VarInfoPtr,GADTResult)
	get_next_gadt_case_in_next_case ap_expr opt_next_gadt_case case_var_info_ptrs tb_args common_defs
		| not (is_gadt_case ap_expr common_defs)
			= No
		# (Case {case_expr=Var {var_info_ptr},case_guards=AlgebraicPatterns _ next_case_patterns}) = ap_expr
		= case opt_next_gadt_case of
			No
				| is_in_var_info_ptrs case_var_info_ptrs var_info_ptr
					-> No
				# arg_n = get_gadt_case_argument_n tb_args ap_expr
				| arg_n<0
					-> No
				-> case get_gadt_case_type_and_vars next_case_patterns No common_defs of
					No
						-> No
					Yes gadt_case_type_and_vars
						-> Yes (var_info_ptr,gadt_case_type_and_vars)
			Yes (previous_var_info_ptr,gadt_case_type_and_vars)
				| var_info_ptr<>previous_var_info_ptr
					-> No
				-> case get_gadt_case_type_and_vars next_case_patterns (Yes gadt_case_type_and_vars) common_defs of
					No
						-> No
					Yes gadt_case_type_and_vars
						-> Yes (previous_var_info_ptr,gadt_case_type_and_vars)

	is_in_var_info_ptrs :: ![VarInfoPtr] !VarInfoPtr -> Bool
	is_in_var_info_ptrs [var_info_ptrs_elem:var_info_ptrs] var_info_ptr
		= var_info_ptrs_elem==var_info_ptr || is_in_var_info_ptrs var_info_ptrs var_info_ptr
	is_in_var_info_ptrs [] var_info_ptr
		= False

fresh_gadt_type_variables_and_attributes :: !AType ![ATypeVar] ![TypeVar] ![AttributeVar] !*TypeHeaps -> (![TypeVar],!*TypeHeaps)
fresh_gadt_type_variables_and_attributes gadt_type gadt_adt_type_vars gadt_type_vars gadt_attr_vars type_heaps
	# (new_gadt_type_vars,new_gadt_attr_vars,type_heaps) = fresh_a_type_variables gadt_adt_type_vars type_heaps
	  (new_gadt_type_vars,th_vars) = fresh_type_variables gadt_type_vars new_gadt_type_vars type_heaps.th_vars
	  th_attrs = case gadt_type.at_attribute of
					TA_Var av -> snd (fresh_attribute_variable av type_heaps.th_attrs)
					_ -> type_heaps.th_attrs
	  (new_gadt_attr_vars,th_attrs) = fresh_attribute_variables gadt_attr_vars new_gadt_attr_vars th_attrs
	  type_heaps & th_vars=th_vars,th_attrs=th_attrs
	= (new_gadt_type_vars,type_heaps)
where
	fresh_a_type_variables :: ![ATypeVar] !*TypeHeaps -> (![TypeVar],![AttributeVar],!*TypeHeaps)
	fresh_a_type_variables [{atv_attribute=TA_Var av,atv_variable}:tvs] type_heaps
		# (new_tv,th_vars) = fresh_type_variable atv_variable type_heaps.th_vars
		  (new_av,th_attrs) = fresh_attribute_variable av type_heaps.th_attrs
		  type_heaps & th_vars=th_vars,th_attrs=th_attrs
		  (new_tvs,new_avs,type_heaps) = fresh_a_type_variables tvs type_heaps
		= ([new_tv:new_tvs],[new_av:new_avs],type_heaps)
	fresh_a_type_variables [{atv_variable}:tvs] type_heaps
		# (new_tv,th_vars) = fresh_type_variable atv_variable type_heaps.th_vars
		  type_heaps & th_vars=th_vars
		  (new_tvs,new_avs,type_heaps) = fresh_a_type_variables tvs type_heaps
		= ([new_tv:new_tvs],new_avs,type_heaps)
	fresh_a_type_variables [] type_heaps
		= ([],[],type_heaps)

	fresh_type_variables :: ![TypeVar] ![TypeVar] !*TypeVarHeap -> (![TypeVar],!*TypeVarHeap)
	fresh_type_variables [tv:tvs] previous_tvs var_heap
		# (new_tv,var_heap) = fresh_type_variable tv var_heap
		  (new_tvs,var_heap) = fresh_type_variables tvs previous_tvs var_heap
		= ([new_tv:new_tvs],var_heap)
	fresh_type_variables [] previous_tvs var_heap
		= (previous_tvs,var_heap)

	fresh_type_variable :: !TypeVar !*TypeVarHeap -> (!TypeVar,!*TypeVarHeap)
	fresh_type_variable tv=:{tv_info_ptr} var_heap
		# (new_tv_info_ptr,var_heap) = newPtr TVI_Empty var_heap
		  new_tv = {tv & tv_info_ptr=new_tv_info_ptr}
		  var_heap = writePtr tv_info_ptr (TVI_Type (TV new_tv)) var_heap
		= (new_tv,var_heap)

	fresh_attribute_variables :: ![AttributeVar] ![AttributeVar] !*AttrVarHeap -> (![AttributeVar],!*AttrVarHeap)
	fresh_attribute_variables [av:avs] previous_avs attr_heap
		# (new_av,attr_heap) = fresh_attribute_variable av attr_heap
		  (new_tvs,attr_heap) = fresh_attribute_variables avs previous_avs attr_heap
		= ([new_av:new_tvs],attr_heap)
	fresh_attribute_variables [] previous_avs attr_heap
		= (previous_avs,attr_heap)

	fresh_attribute_variable :: !AttributeVar !*AttrVarHeap -> (!AttributeVar,!*AttrVarHeap)
	fresh_attribute_variable av=:{av_info_ptr} attr_heap
		# (new_av_info_ptr,attr_heap) = newPtr AVI_Empty attr_heap
		  new_av = {av & av_info_ptr=new_av_info_ptr}
		  attr_heap = writePtr av_info_ptr (AVI_Attr (TA_Var new_av)) attr_heap
		= (new_av,attr_heap)

clear_gadt_type_variables_and_attributes :: !AType ![TypeVar] ![AttributeVar] !*TypeHeaps -> *TypeHeaps
clear_gadt_type_variables_and_attributes gadt_type gadt_type_vars gadt_attr_vars type_heaps
	# th_vars = clear_type_variables gadt_type_vars type_heaps.th_vars
	  th_attrs = case gadt_type.at_attribute of
					TA_Var av -> clear_attribute_variable av type_heaps.th_attrs
					_ -> type_heaps.th_attrs
	  th_attrs = clear_attribute_variables gadt_attr_vars th_attrs
	= {type_heaps & th_vars=th_vars,th_attrs=th_attrs}

fresh_refined_type_vars_in_gadt_type :: !Type !Int !*TypeVarHeap !*AttrVarHeap -> (!Int,!Type,!*TypeVarHeap,!*AttrVarHeap)
fresh_refined_type_vars_in_gadt_type type=:(TB _) errors th_vars th_attrs
	= (errors,type,th_vars,th_attrs)
fresh_refined_type_vars_in_gadt_type (TV tv=:{tv_info_ptr}) errors th_vars th_attrs
	# (tvi,th_vars) = readPtr tv_info_ptr th_vars
	= case tvi of
		TVI_Empty
			# (new_tv_info_ptr,th_vars) = newPtr TVI_Empty th_vars
			  new_tv = TV {tv & tv_info_ptr=new_tv_info_ptr}
			  th_vars = writePtr tv_info_ptr (TVI_SubstSpecifiedForGADType new_tv) th_vars
			-> (errors,new_tv,th_vars,th_attrs)
		TVI_SubstSpecifiedForGADType specified_gadt_type
			-> (errors,specified_gadt_type,th_vars,th_attrs)
fresh_refined_type_vars_in_gadt_type (TA tsi atypes) errors th_vars th_attrs
	# (errors,atypes,th_vars,th_attrs) = fresh_refined_type_vars_in_gadt_atypes atypes errors th_vars th_attrs
	= (errors,TA tsi atypes,th_vars,th_attrs)
fresh_refined_type_vars_in_gadt_type (TAS tsi atypes sl) errors th_vars th_attrs
	# (errors,atypes,th_vars,th_attrs) = fresh_refined_type_vars_in_gadt_atypes atypes errors th_vars th_attrs
	= (errors,TAS tsi atypes sl,th_vars,th_attrs)
fresh_refined_type_vars_in_gadt_type (CV cv=:{tv_info_ptr} :@: atypes) errors th_vars th_attrs
	# (errors,atypes,th_vars,th_attrs) = fresh_refined_type_vars_in_gadt_atypes atypes errors th_vars th_attrs
	# (tvi,th_vars) = readPtr tv_info_ptr th_vars
	= case tvi of
		TVI_Empty
			# (new_tv_info_ptr,th_vars) = newPtr TVI_Empty th_vars
			  new_cv = {cv & tv_info_ptr=new_tv_info_ptr}
			  th_vars = writePtr tv_info_ptr (TVI_SubstSpecifiedForGADType (TV new_cv)) th_vars
			-> (errors,CV new_cv :@: atypes,th_vars,th_attrs)
		TVI_SubstSpecifiedForGADType (TV specified_gadt_tv)
			-> (errors,CV specified_gadt_tv :@: atypes,th_vars,th_attrs)
		TVI_SubstSpecifiedForGADType specified_gadt_type
			-> (errors bitor 2,CV cv :@: atypes,th_vars,th_attrs)
fresh_refined_type_vars_in_gadt_type (atype1-->atype2) errors th_vars th_attrs
	# (errors,atype1,th_vars,th_attrs) = fresh_refined_type_vars_in_gadt_atype atype1 errors th_vars th_attrs
	# (errors,atype2,th_vars,th_attrs) = fresh_refined_type_vars_in_gadt_atype atype2 errors th_vars th_attrs
	= (errors,atype1-->atype2,th_vars,th_attrs)
fresh_refined_type_vars_in_gadt_type (TArrow1 atype1) errors th_vars th_attrs
	# (errors,atype1,th_vars,th_attrs) = fresh_refined_type_vars_in_gadt_atype atype1 errors th_vars th_attrs
	= (errors,TArrow1 atype1,th_vars,th_attrs)
fresh_refined_type_vars_in_gadt_type type=:TArrow errors th_vars th_attrs
	= (errors,type,th_vars,th_attrs)
fresh_refined_type_vars_in_gadt_type type errors th_vars th_attrs
	= (errors bitor 2,type,th_vars,th_attrs)

fresh_refined_type_vars_in_gadt_atype :: !AType !Int !*TypeVarHeap !*AttrVarHeap -> (!Int,!AType,!*TypeVarHeap,!*AttrVarHeap)
fresh_refined_type_vars_in_gadt_atype atype=:{at_attribute,at_type} errors th_vars th_attrs
	# (errors,at_type,th_vars,th_attrs) = fresh_refined_type_vars_in_gadt_type at_type errors th_vars th_attrs
	= case at_attribute of
		TA_Var {av_info_ptr,av_ident}
			# (av_info, th_attrs) = readPtr av_info_ptr th_attrs
			-> case av_info of
				AVI_Attr attribute
					-> (errors,{atype & at_attribute=attribute,at_type=at_type},th_vars,th_attrs)
				_
					-> (errors bitor 1,{atype & at_type=at_type},th_vars,th_attrs)
		TA_Multi
			-> (errors,{atype & at_type=at_type},th_vars,th_attrs)
		TA_Unique
			-> (errors,{atype & at_type=at_type},th_vars,th_attrs)
		_
			-> (errors bitor 1,{atype & at_type=at_type},th_vars,th_attrs)

fresh_refined_type_vars_in_gadt_atypes :: ![AType] !Int !*TypeVarHeap !*AttrVarHeap -> (!Int,![AType],!*TypeVarHeap,!*AttrVarHeap)
fresh_refined_type_vars_in_gadt_atypes [atype:atypes] errors th_vars th_attrs
	# (errors,atype,th_vars,th_attrs) = fresh_refined_type_vars_in_gadt_atype atype errors th_vars th_attrs
	# (errors,atypes,th_vars,th_attrs) = fresh_refined_type_vars_in_gadt_atypes atypes errors th_vars th_attrs
	= (errors,[atype:atypes],th_vars,th_attrs)
fresh_refined_type_vars_in_gadt_atypes [] errors th_vars th_attrs
	= (errors,[],th_vars,th_attrs)

class contains_type_variable a :: !TypeVarInfoPtr !a !TypeVarHeap -> Bool

instance contains_type_variable AType where
	contains_type_variable tvip {at_type} tvh
		= contains_type_variable tvip at_type tvh

instance contains_type_variable TypeVarInfoPtr where
	contains_type_variable tvip tv_info_ptr tvh
		| tv_info_ptr==tvip
			= True
		= case sreadPtr tv_info_ptr tvh of
			TVI_SubstSpecifiedForGADType type
				-> contains_type_variable tvip type tvh
			_
				-> False

instance contains_type_variable Type where
	contains_type_variable tvip (TV {tv_info_ptr}) tvh
		= contains_type_variable tvip tv_info_ptr tvh
	contains_type_variable tvip (arg_type --> res_type) tvh
		= contains_type_variable tvip arg_type tvh || contains_type_variable tvip res_type tvh
	contains_type_variable tvip (TA cons_id cons_args) tvh
		= contains_type_variable tvip cons_args tvh
	contains_type_variable tvip (TAS cons_id cons_args _) tvh
		= contains_type_variable tvip cons_args tvh
	contains_type_variable tvip (type :@: types) tvh
		= contains_type_variable tvip type tvh || contains_type_variable tvip types tvh
	contains_type_variable tvip (TArrow1 arg_type) tvh
		= contains_type_variable tvip arg_type tvh
	contains_type_variable _ _ _
		= False

instance contains_type_variable ConsVariable where
	contains_type_variable tvip (CV {tv_info_ptr}) tvh
		= contains_type_variable tvip tv_info_ptr tvh
	contains_type_variable tvip _ _
		= False

instance contains_type_variable [a] | contains_type_variable a where
	contains_type_variable tvip [elem:list] tvh
		= contains_type_variable tvip elem tvh || contains_type_variable tvip list tvh
	contains_type_variable tvip [] _
		= False

unify_specified_gadt_atype :: !AType !AType !Int !*TypeVarHeap !*AttrVarHeap -> (!Int,!*TypeVarHeap,!*AttrVarHeap)
unify_specified_gadt_atype {at_attribute=attribute1,at_type=type1} {at_attribute=attribute2,at_type=type2} errors th_vars th_attrs
	# (attribute_errors,th_attrs) = unify_specified_gadt_type_attribute attribute1 attribute2 th_attrs
	# errors = errors bitor attribute_errors
	= unify_specified_gadt_type type1 type2 errors th_vars th_attrs
where
	unify_specified_gadt_type_attribute :: !TypeAttribute !TypeAttribute !*AttrVarHeap -> (!Int,!*AttrVarHeap)
	unify_specified_gadt_type_attribute TA_Multi TA_Multi attr_var_heap
		= (0,attr_var_heap)
	unify_specified_gadt_type_attribute TA_Multi (TA_Var {av_info_ptr}) attr_var_heap
		= unify_specified_gadt_type_attribute_var_multi av_info_ptr attr_var_heap
	unify_specified_gadt_type_attribute TA_Unique TA_Unique attr_var_heap
		= (0,attr_var_heap)
	unify_specified_gadt_type_attribute TA_Unique (TA_Var {av_info_ptr}) attr_var_heap
		= unify_specified_gadt_type_attribute_var_unique av_info_ptr attr_var_heap
	unify_specified_gadt_type_attribute (TA_Var {av_info_ptr}) TA_Multi attr_var_heap
		= unify_specified_gadt_type_attribute_var_multi av_info_ptr attr_var_heap
	unify_specified_gadt_type_attribute (TA_Var {av_info_ptr}) TA_Unique attr_var_heap
		= unify_specified_gadt_type_attribute_var_unique av_info_ptr attr_var_heap
	unify_specified_gadt_type_attribute attribute1 attribute2 attr_var_heap
		= (1,attr_var_heap)

	unify_specified_gadt_type_attribute_var_multi :: !AttrVarInfoPtr !*AttrVarHeap -> (!Int,!*AttrVarHeap)
	unify_specified_gadt_type_attribute_var_multi av_info_ptr attr_var_heap
		# (av_info, attr_var_heap) = readPtr av_info_ptr attr_var_heap
		= case av_info of
			AVI_Attr TA_Multi
				-> (0,attr_var_heap)
			AVI_Empty
				# attr_var_heap = writePtr av_info_ptr (AVI_Attr TA_Multi) attr_var_heap
				-> (0,attr_var_heap)
			_
				-> (1,attr_var_heap)

	unify_specified_gadt_type_attribute_var_unique :: !AttrVarInfoPtr !*AttrVarHeap -> (!Int,!*AttrVarHeap)
	unify_specified_gadt_type_attribute_var_unique av_info_ptr attr_var_heap
		# (av_info, attr_var_heap) = readPtr av_info_ptr attr_var_heap
		= case av_info of
			AVI_Attr TA_Unique
				-> (0,attr_var_heap)
			AVI_Empty
				# attr_var_heap = writePtr av_info_ptr (AVI_Attr TA_Unique) attr_var_heap
				-> (0,attr_var_heap)
			_
				-> (1,attr_var_heap)

unify_specified_gadt_type :: !Type !Type !Int !*TypeVarHeap !*AttrVarHeap -> (!Int,!*TypeVarHeap,!*AttrVarHeap)
unify_specified_gadt_type (TV {tv_info_ptr=tv_info_ptr1}) (TV {tv_info_ptr=tv_info_ptr2}) errors th_vars th_attrs
	| tv_info_ptr1==tv_info_ptr2
		= (errors,th_vars,th_attrs)
unify_specified_gadt_type (TV {tv_info_ptr}) type2 errors th_vars th_attrs
	= unify_specified_gadt_no_subst_tv_and_type tv_info_ptr type2 errors th_vars th_attrs
unify_specified_gadt_type (TA tsi1 atypes1) (TA tsi2 atypes2) errors th_vars th_attrs
	| tsi1==tsi2
		= unify_specified_gadt_atypes atypes1 atypes2 errors th_vars th_attrs
		= (errors bitor 2,th_vars,th_attrs)
unify_specified_gadt_type (TAS tsi1 atypes1 _) (TAS tsi2 atypes2 _) errors th_vars th_attrs
	| tsi1==tsi2
		= unify_specified_gadt_atypes atypes1 atypes2 errors th_vars th_attrs
		= (errors bitor 2,th_vars,th_attrs)
unify_specified_gadt_type (atype1a-->atype1r) (atype2a-->atype2r) errors th_vars th_attrs
	# (errors,th_vars,th_attrs) = unify_specified_gadt_atype atype1a atype2a errors th_vars th_attrs
	= unify_specified_gadt_atype atype1r atype2r errors th_vars th_attrs
unify_specified_gadt_type (TB tb1) (TB tb2) errors th_vars th_attrs
	| tb1==tb2
		= (errors,th_vars,th_attrs)
		= (errors bitor 2,th_vars,th_attrs)
unify_specified_gadt_type (cv1=:(CV _) :@: atypes1) type2 errors th_vars th_attrs
	= unify_specified_gadt_cv_application_and_type cv1 atypes1 type2 errors th_vars th_attrs
unify_specified_gadt_type type1 (cv2=:(CV _) :@: atypes2) errors th_vars th_attrs
	= unify_specified_gadt_cv_application_and_type cv2 atypes2 type1 errors th_vars th_attrs
unify_specified_gadt_type (TArrow1 atype1) (TArrow1 atype2) errors th_vars th_attrs
	= unify_specified_gadt_atype atype1 atype2 errors th_vars th_attrs
unify_specified_gadt_type TArrow TArrow errors th_vars th_attrs
	= (errors,th_vars,th_attrs)
unify_specified_gadt_type type1 (TV {tv_info_ptr}) errors th_vars th_attrs
	= unify_specified_gadt_no_subst_tv_and_type tv_info_ptr type1 errors th_vars th_attrs
unify_specified_gadt_type type1 type2 errors th_vars th_attrs
	= (errors bitor 2,th_vars,th_attrs)

unify_specified_gadt_no_subst_tv_and_type :: !TypeVarInfoPtr !Type !Int !*TypeVarHeap !*AttrVarHeap -> (!Int,!*TypeVarHeap,!*AttrVarHeap)
unify_specified_gadt_no_subst_tv_and_type tv_info_ptr1 type2=:(TV {tv_info_ptr=tv_info_ptr2}) errors th_vars th_attrs
	| tv_info_ptr1==tv_info_ptr2
		= (errors,th_vars,th_attrs)
	# (tv2,th_vars) = readPtr tv_info_ptr2 th_vars
	= case tv2 of
		TVI_Empty
			# th_vars = writePtr tv_info_ptr1 (TVI_SubstSpecifiedType type2) th_vars
			-> (errors,th_vars,th_attrs)
		TVI_SubstSpecifiedType type2
			-> unify_specified_gadt_no_subst_tv_and_type tv_info_ptr1 type2 errors th_vars th_attrs
unify_specified_gadt_no_subst_tv_and_type tv_info_ptr1 type2 errors th_vars th_attrs
	= unify_specified_gadt_no_subst_tv_and_no_tv_type tv_info_ptr1 type2 errors th_vars th_attrs

unify_specified_gadt_no_subst_tv_and_no_tv_type :: !TypeVarInfoPtr !Type !Int !*TypeVarHeap !*AttrVarHeap -> (!Int,!*TypeVarHeap,!*AttrVarHeap)
unify_specified_gadt_no_subst_tv_and_no_tv_type tv_info_ptr1 type2 errors th_vars th_attrs
	| contains_type_variable tv_info_ptr1 type2 th_vars
		= (errors bitor 2,th_vars,th_attrs)
		# th_vars = writePtr tv_info_ptr1 (TVI_SubstSpecifiedType type2) th_vars
		= (errors,th_vars,th_attrs)

unify_specified_gadt_atypes :: ![AType] ![AType] !Int !*TypeVarHeap !*AttrVarHeap -> (!Int,!*TypeVarHeap,!*AttrVarHeap)
unify_specified_gadt_atypes [atype1:atypes1] [atype2:atypes2] errors th_vars th_attrs
	# (errors,th_vars,th_attrs) = unify_specified_gadt_atype atype1 atype2 errors th_vars th_attrs
	= unify_specified_gadt_atypes atypes1 atypes2 errors th_vars th_attrs
unify_specified_gadt_atypes [] [] errors th_vars th_attrs
	= (errors,th_vars,th_attrs)
unify_specified_gadt_atypes types1 types2 errors th_vars th_attrs
	= (errors bitor 2,th_vars,th_attrs)

unify_specified_gadt_cv_application_and_type :: !ConsVariable ![AType] !Type !Int !*TypeVarHeap !*AttrVarHeap -> (!Int,!*TypeVarHeap,!*AttrVarHeap)
unify_specified_gadt_cv_application_and_type cv1=:(CV {tv_info_ptr=tv_info_ptr1}) atypes1 type2 errors th_vars th_attrs
	# (tvi1,th_vars) = readPtr tv_info_ptr1 th_vars
	= case tvi1 of
		TVI_Empty
			-> unify_specified_gadt_no_subst_cv_application_and_type cv1 atypes1 type2 errors th_vars th_attrs
		TVI_SubstSpecifiedType specified_gadt_type1
			# (ok, simplified_type1) = simplifyAndCheckTypeApplication specified_gadt_type1 atypes1
			| ok
				-> unify_specified_gadt_type simplified_type1 type2 errors th_vars th_attrs
				-> (errors bitor 2,th_vars,th_attrs)

unify_specified_gadt_no_subst_cv_application_and_type :: !ConsVariable ![AType] !Type !Int !*TypeVarHeap !*AttrVarHeap -> (!Int,!*TypeVarHeap,!*AttrVarHeap)
unify_specified_gadt_no_subst_cv_application_and_type cv1=:(CV tv1=:{tv_info_ptr=tv_info_ptr1}) atypes1 (cv2=:(CV tv2=:{tv_info_ptr=tv_info_ptr2}) :@: atypes2) errors th_vars th_attrs
	# (tvi2,th_vars) = readPtr tv_info_ptr2 th_vars
	= case tvi2 of
		TVI_Empty
			# n_atypes1 = length atypes1
			  n_atypes2 = length atypes2
			| n_atypes1==n_atypes2
				| tv_info_ptr1==tv_info_ptr2
					-> unify_specified_gadt_atypes atypes1 atypes2 errors th_vars th_attrs
					# th_vars = writePtr tv_info_ptr1 (TVI_SubstSpecifiedType (TV tv2)) th_vars
					-> unify_specified_gadt_atypes atypes1 atypes2 errors th_vars th_attrs
			| n_atypes1>n_atypes2
				# diff = n_atypes1 - n_atypes2
				  (errors,th_vars,th_attrs) = unify_specified_gadt_atypes (drop diff atypes1) atypes2 errors th_vars th_attrs
				-> unify_specified_gadt_type (cv1 :@: take diff atypes1) (TV tv2) errors th_vars th_attrs
				# diff = n_atypes2 - n_atypes1
				  (errors,th_vars,th_attrs) = unify_specified_gadt_atypes atypes1 (drop diff atypes2) errors th_vars th_attrs
				-> unify_specified_gadt_type (TV tv1) (cv2 :@: take diff atypes2) errors th_vars th_attrs
		TVI_SubstSpecifiedType specified_gadt_type2
			# (ok, simplified_type2) = simplifyAndCheckTypeApplication specified_gadt_type2 atypes2
			| ok
				-> unify_specified_gadt_no_subst_cv_application_and_type cv1 atypes1 simplified_type2 errors th_vars th_attrs
				-> (errors bitor 2,th_vars,th_attrs)
unify_specified_gadt_no_subst_cv_application_and_type (CV {tv_info_ptr=tv_info_ptr1}) atypes1 (TA tsi2 atypes2) errors th_vars th_attrs
	# diff = tsi2.type_arity - length atypes1
	| diff>=0
		# (errors,th_vars,th_attrs) = unify_specified_gadt_atypes atypes1 (drop diff atypes2) errors th_vars th_attrs
		# type2 = TA {tsi2 & type_arity=diff} (take diff atypes2)
		= unify_specified_gadt_no_subst_tv_and_no_tv_type tv_info_ptr1 type2 errors th_vars th_attrs
		= (errors bitor 2,th_vars,th_attrs)
unify_specified_gadt_no_subst_cv_application_and_type (CV {tv_info_ptr=tv_info_ptr1}) atypes1 (TAS tsi2 atypes2 strictness) errors th_vars th_attrs
	# diff = tsi2.type_arity - length atypes1
	| diff>=0
		# (errors,th_vars,th_attrs) = unify_specified_gadt_atypes atypes1 (drop diff atypes2) errors th_vars th_attrs
		# type2 = TAS {tsi2 & type_arity=diff} (take diff atypes2) strictness
		= unify_specified_gadt_no_subst_tv_and_no_tv_type tv_info_ptr1 type2 errors th_vars th_attrs
		= (errors bitor 2,th_vars,th_attrs)
unify_specified_gadt_no_subst_cv_application_and_type (CV {tv_info_ptr=tv_info_ptr1}) [atype11,atype12] (atype21 --> atype22) errors th_vars th_attrs
	# th_vars = writePtr tv_info_ptr1 (TVI_SubstSpecifiedType TArrow) th_vars
	  (errors,th_vars,th_attrs) = unify_specified_gadt_atype atype11 atype21 errors th_vars th_attrs
	= unify_specified_gadt_atype atype12 atype22 errors th_vars th_attrs
unify_specified_gadt_no_subst_cv_application_and_type (CV {tv_info_ptr=tv_info_ptr1}) [atype1] (atype21 --> atype22) errors th_vars th_attrs
	# (errors,th_vars,th_attrs) = unify_specified_gadt_no_subst_tv_and_no_tv_type tv_info_ptr1 (TArrow1 atype21) errors th_vars th_attrs
	= unify_specified_gadt_atype atype1 atype22 errors th_vars th_attrs
unify_specified_gadt_no_subst_cv_application_and_type (CV {tv_info_ptr=tv_info_ptr1}) [atype1] (TArrow1 atype2) errors th_vars th_attrs
	# th_vars = writePtr tv_info_ptr1 (TVI_SubstSpecifiedType TArrow) th_vars
	= unify_specified_gadt_atype atype1 atype2 errors th_vars th_attrs
unify_specified_gadt_no_subst_cv_application_and_type cv1 atypes1 type2 errors th_vars th_attrs
	= (errors bitor 2,th_vars,th_attrs)

set_atype_gadt_refinements :: !AType !AType !Int !*TypeVarHeap !*AttrVarHeap -> (!Int,!*TypeVarHeap,!*AttrVarHeap)
set_atype_gadt_refinements {at_attribute=specified_attribute,at_type=specified_type} {at_attribute=gadt_attribute,at_type=gadt_type} errors th_vars th_attrs
	# (attribute_errors,th_attrs) = set_type_attribute_gadt_refinements specified_attribute gadt_attribute th_attrs
	= set_type_gadt_refinements specified_type gadt_type (errors bitor attribute_errors) th_vars th_attrs
where
	set_type_attribute_gadt_refinements :: !TypeAttribute !TypeAttribute !*AttrVarHeap -> (!Int,!*AttrVarHeap)
	set_type_attribute_gadt_refinements TA_Multi TA_Multi attr_var_heap
		= (0,attr_var_heap)
	set_type_attribute_gadt_refinements TA_Multi gadt_attribute=:(TA_Var {av_info_ptr}) attr_var_heap
		# (av_info, attr_var_heap) = readPtr av_info_ptr attr_var_heap
		= case av_info of
			AVI_Attr TA_Multi
				-> (0,attr_var_heap)
			_
				-> (1,attr_var_heap)
	set_type_attribute_gadt_refinements TA_Unique TA_Unique attr_var_heap
		= (0,attr_var_heap)
	set_type_attribute_gadt_refinements TA_Unique gadt_attribute=:(TA_Var {av_info_ptr}) attr_var_heap
		# (av_info, attr_var_heap) = readPtr av_info_ptr attr_var_heap
		= case av_info of
			AVI_Attr TA_Unique
				-> (0,attr_var_heap)
			_
				-> (1,attr_var_heap)
	set_type_attribute_gadt_refinements specified_attribute=:(TA_Var {av_info_ptr}) TA_Multi attr_var_heap
		# (av_info, attr_var_heap) = readPtr av_info_ptr attr_var_heap
		= case av_info of
			AVI_Attr TA_Multi
				-> (0,attr_var_heap)
			AVI_Empty
				-> (1,attr_var_heap)
			_
				-> (1,attr_var_heap)
	set_type_attribute_gadt_refinements specified_attribute=:(TA_Var {av_info_ptr}) TA_Unique attr_var_heap
		# (av_info, attr_var_heap) = readPtr av_info_ptr attr_var_heap
		= case av_info of
			AVI_Attr TA_Unique
				-> (0,attr_var_heap)
			_
				-> (1,attr_var_heap)
	set_type_attribute_gadt_refinements specified_attribute=:(TA_Var {av_info_ptr=av_info_ptr1}) gadt_attribute=:(TA_Var {av_info_ptr=av_info_ptr2}) attr_var_heap
		# (av_info2, attr_var_heap) = readPtr av_info_ptr2 attr_var_heap
		= case av_info2 of
			AVI_Attr (TA_Var {av_info_ptr})
				| av_info_ptr==av_info_ptr1
					-> (0,attr_var_heap)
			_
				-> (1,attr_var_heap)
	set_type_attribute_gadt_refinements specified_attribute=:(TA_Var {av_info_ptr=av_info_ptr1}) gadt_attribute=:(TA_RootVar {av_info_ptr=av_info_ptr2}) attr_var_heap
		= (1,attr_var_heap)
	set_type_attribute_gadt_refinements specified_attribute gadt_attribute attr_var_heap
		= (1,attr_var_heap)

set_type_gadt_refinements :: !Type !Type !Int !*TypeVarHeap !*AttrVarHeap -> (!Int,!*TypeVarHeap,!*AttrVarHeap)
set_type_gadt_refinements specified_type (TV tv) errors th_vars th_attrs
	= set_gadt_tv_gadt_refinements specified_type tv errors th_vars th_attrs
set_type_gadt_refinements (TV tv) gadt_type errors th_vars th_attrs
	= set_specified_tv_gadt_refinements tv gadt_type errors th_vars th_attrs
set_type_gadt_refinements (TA specified_tsi specified_atypes) (TA gadt_tsi gadt_atypes) errors th_vars th_attrs
	| specified_tsi==gadt_tsi
		= set_atypes_gadt_refinements specified_atypes gadt_atypes errors th_vars th_attrs
		= (errors bitor 2,th_vars,th_attrs)
set_type_gadt_refinements (TAS specified_tsi specified_atypes _) (TAS gadt_tsi gadt_atypes _) errors th_vars th_attrs
	| specified_tsi==gadt_tsi
		= set_atypes_gadt_refinements specified_atypes gadt_atypes errors th_vars th_attrs
		= (errors bitor 2,th_vars,th_attrs)
set_type_gadt_refinements (specified_atype1-->specified_atype2) (gadt_atype1-->gadt_atype2) errors th_vars th_attrs
	# (errors,th_vars,th_attrs) = set_atype_gadt_refinements specified_atype1 gadt_atype1 errors th_vars th_attrs
	= set_atype_gadt_refinements specified_atype2 gadt_atype2 errors th_vars th_attrs
set_type_gadt_refinements (TB tb1) (TB tb2) errors th_vars th_attrs
	| tb1==tb2
		= (errors,th_vars,th_attrs)
		= (errors bitor 2,th_vars,th_attrs)
set_type_gadt_refinements specified_type (CV gadt_cv :@: gadt_atypes) errors th_vars th_attrs
	= set_gadt_cv_application_gadt_refinements specified_type gadt_cv gadt_atypes errors th_vars th_attrs
set_type_gadt_refinements (CV specified_cv :@: specified_atypes) gadt_type  errors th_vars th_attrs
	= set_specified_cv_application_gadt_refinements specified_cv specified_atypes gadt_type errors th_vars th_attrs
set_type_gadt_refinements (TArrow1 specified_atype) (TArrow1 gadt_atype) errors th_vars th_attrs
	= set_atype_gadt_refinements specified_atype gadt_atype errors th_vars th_attrs
set_type_gadt_refinements TArrow TArrow errors th_vars th_attrs
	= (errors,th_vars,th_attrs)
set_type_gadt_refinements type1 type2 errors th_vars th_attrs
	= (errors bitor 2,th_vars,th_attrs)

set_gadt_tv_gadt_refinements :: !Type !TypeVar !Int !*TypeVarHeap !*AttrVarHeap -> (!Int,!*TypeVarHeap,!*AttrVarHeap)
set_gadt_tv_gadt_refinements specified_type {tv_info_ptr,tv_ident} errors th_vars th_attrs
	# (tv,th_vars) = readPtr tv_info_ptr th_vars
	= case tv of
		TVI_Empty
			# th_vars = writePtr tv_info_ptr (TVI_SubstSpecifiedForGADType specified_type) th_vars
			-> (errors,th_vars,th_attrs)
		TVI_SubstSpecifiedForGADType specified_type2
			-> unify_specified_gadt_type specified_type2 specified_type errors th_vars th_attrs

set_specified_tv_gadt_refinements :: !TypeVar !Type  !Int !*TypeVarHeap !*AttrVarHeap -> (!Int,!*TypeVarHeap,!*AttrVarHeap)
set_specified_tv_gadt_refinements {tv_info_ptr,tv_ident} gadt_type errors th_vars th_attrs
	# (tv,th_vars) = readPtr tv_info_ptr th_vars
	= case tv of
		TVI_Empty
			# (fresh_errors,gadt_type,th_vars,th_attrs) = fresh_refined_type_vars_in_gadt_type gadt_type 0 th_vars th_attrs
			| fresh_errors<>0
				-> (errors bitor fresh_errors,th_vars,th_attrs)
			# th_vars = writePtr tv_info_ptr (TVI_SubstSpecifiedType gadt_type) th_vars
			-> (errors,th_vars,th_attrs)
		TVI_SubstSpecifiedType specified_type2
			-> set_type_gadt_refinements specified_type2 gadt_type errors th_vars th_attrs

set_atypes_gadt_refinements :: ![AType] ![AType] !Int !*TypeVarHeap !*AttrVarHeap -> (!Int,!*TypeVarHeap,!*AttrVarHeap)
set_atypes_gadt_refinements [specified_atype:specified_atypes] [gadt_atype:gadt_atypes] errors th_vars th_attrs
	# (errors,th_vars,th_attrs) = set_atype_gadt_refinements specified_atype gadt_atype errors th_vars th_attrs
	= set_atypes_gadt_refinements specified_atypes gadt_atypes errors th_vars th_attrs
set_atypes_gadt_refinements [] [] errors th_vars th_attrs
	= (errors,th_vars,th_attrs)
set_atypes_gadt_refinements types1 types2 errors th_vars th_attrs
	= (errors bitor 2,th_vars,th_attrs)

set_gadt_cv_application_gadt_refinements :: !Type !TypeVar ![AType] !Int !*TypeVarHeap !*AttrVarHeap -> (!Int,!*TypeVarHeap,!*AttrVarHeap)
set_gadt_cv_application_gadt_refinements (CV specified_cv :@: specified_atypes) gadt_cv gadt_atypes errors th_vars th_attrs
	# n_specified_atypes = length specified_atypes
	  n_gadt_atypes = length gadt_atypes
	| n_specified_atypes==n_gadt_atypes
		# (errors,th_vars,th_attrs) = set_atypes_gadt_refinements specified_atypes gadt_atypes errors th_vars th_attrs
		= set_gadt_tv_gadt_refinements (TV specified_cv) gadt_cv errors th_vars th_attrs
	| n_specified_atypes<n_gadt_atypes
		# diff = n_gadt_atypes - n_specified_atypes
		  (errors,th_vars,th_attrs) = set_atypes_gadt_refinements specified_atypes (drop diff gadt_atypes) errors th_vars th_attrs
		= set_type_gadt_refinements (TV specified_cv) (CV gadt_cv :@: take diff gadt_atypes) errors th_vars th_attrs
		# diff = n_specified_atypes - n_gadt_atypes
		  (errors,th_vars,th_attrs) = set_atypes_gadt_refinements (drop diff specified_atypes) gadt_atypes errors th_vars th_attrs
		= set_type_gadt_refinements (CV specified_cv :@: take diff specified_atypes) (TV gadt_cv) errors th_vars th_attrs
set_gadt_cv_application_gadt_refinements (TA specified_tsi specified_atypes) gadt_cv gadt_atypes errors th_vars th_attrs
	# diff = specified_tsi.type_arity - length gadt_atypes
	| diff>=0
		# (errors,th_vars,th_attrs) = set_atypes_gadt_refinements (drop diff specified_atypes) gadt_atypes errors th_vars th_attrs
		  specified_type = TA {specified_tsi & type_arity=diff} (take diff specified_atypes)
		= set_gadt_tv_gadt_refinements specified_type gadt_cv errors th_vars th_attrs
		= (errors bitor 2,th_vars,th_attrs)
set_gadt_cv_application_gadt_refinements (TAS specified_tsi specified_atypes strictness) gadt_cv gadt_atypes errors th_vars th_attrs
	# diff = specified_tsi.type_arity - length gadt_atypes
	| diff>=0
		# (errors,th_vars,th_attrs) = set_atypes_gadt_refinements (drop diff specified_atypes) gadt_atypes errors th_vars th_attrs
		  specified_type = TAS {specified_tsi & type_arity=diff} (take diff specified_atypes) strictness
		= set_gadt_tv_gadt_refinements specified_type gadt_cv errors th_vars th_attrs
		= (errors bitor 2,th_vars,th_attrs)
set_gadt_cv_application_gadt_refinements (specified_atype1 --> specified_atype2) gadt_cv [gadt_atype1,gadt_atype2] errors th_vars th_attrs
	# (errors,th_vars,th_attrs) = set_gadt_tv_gadt_refinements TArrow gadt_cv errors th_vars th_attrs
	  (errors,th_vars,th_attrs) = set_atype_gadt_refinements specified_atype1 gadt_atype1 errors th_vars th_attrs
	= set_atype_gadt_refinements specified_atype2 gadt_atype2 errors th_vars th_attrs
set_gadt_cv_application_gadt_refinements (specified_atype1 --> specified_atype2) gadt_cv [gadt_atype] errors th_vars th_attrs
	# (errors,th_vars,th_attrs) = set_gadt_tv_gadt_refinements (TArrow1 specified_atype1) gadt_cv errors th_vars th_attrs
	= set_atype_gadt_refinements specified_atype2 gadt_atype errors th_vars th_attrs
set_gadt_cv_application_gadt_refinements (TArrow1 specified_atype) gadt_cv [gadt_atype] errors th_vars th_attrs
	# (errors,th_vars,th_attrs) = set_gadt_tv_gadt_refinements TArrow gadt_cv errors th_vars th_attrs
	= set_atype_gadt_refinements specified_atype gadt_atype errors th_vars th_attrs
set_gadt_cv_application_gadt_refinements specified_type gadt_cv gadt_atypes errors th_vars th_attrs
	= (errors bitor 2,th_vars,th_attrs)

set_specified_cv_application_gadt_refinements :: !TypeVar ![AType] !Type !Int !*TypeVarHeap !*AttrVarHeap -> (!Int,!*TypeVarHeap,!*AttrVarHeap)
set_specified_cv_application_gadt_refinements specified_cv specified_atypes (TA gadt_tsi gadt_atypes) errors th_vars th_attrs
	# n_specified_atypes = length specified_atypes
	| gadt_tsi.type_arity >= n_specified_atypes
		# diff = gadt_tsi.type_arity - n_specified_atypes
		  (errors,th_vars,th_attrs) = set_atypes_gadt_refinements specified_atypes (drop diff gadt_atypes) errors th_vars th_attrs
		  gadt_type = TA {gadt_tsi & type_arity=diff} (take diff gadt_atypes)
		= set_specified_tv_gadt_refinements specified_cv gadt_type errors th_vars th_attrs
		= (errors bitor 2,th_vars,th_attrs)
set_specified_cv_application_gadt_refinements specified_cv specified_atypes (TAS gadt_tsi gadt_atypes strictness) errors th_vars th_attrs
	# n_specified_atypes = length specified_atypes
	| gadt_tsi.type_arity >= n_specified_atypes
		# diff = gadt_tsi.type_arity - n_specified_atypes
		  (errors,th_vars,th_attrs) = set_atypes_gadt_refinements specified_atypes (drop diff gadt_atypes) errors th_vars th_attrs
		  gadt_type = TAS {gadt_tsi & type_arity=diff} (take diff gadt_atypes) strictness
		= set_specified_tv_gadt_refinements specified_cv gadt_type errors th_vars th_attrs
		= (errors bitor 2,th_vars,th_attrs)
set_specified_cv_application_gadt_refinements specified_cv [specified_atype1,specified_atype2] (gadt_atype1 --> gadt_atype2) errors th_vars th_attrs
	# (errors,th_vars,th_attrs) = set_specified_tv_gadt_refinements specified_cv TArrow errors th_vars th_attrs
	  (errors,th_vars,th_attrs) = set_atype_gadt_refinements specified_atype1 gadt_atype1 errors th_vars th_attrs
	= set_atype_gadt_refinements specified_atype2 gadt_atype2 errors th_vars th_attrs
set_specified_cv_application_gadt_refinements specified_cv [specified_atype] (gadt_atype1 --> gadt_atype2) errors th_vars th_attrs
	# (errors,th_vars,th_attrs) = set_specified_tv_gadt_refinements specified_cv (TArrow1 gadt_atype1) errors th_vars th_attrs
	= set_atype_gadt_refinements specified_atype gadt_atype2 errors th_vars th_attrs
set_specified_cv_application_gadt_refinements specified_cv [specified_atype] (TArrow1 gadt_atype) errors th_vars th_attrs
	# (errors,th_vars,th_attrs) = set_specified_tv_gadt_refinements specified_cv TArrow errors th_vars th_attrs
	= set_atype_gadt_refinements specified_atype gadt_atype errors th_vars th_attrs
set_specified_cv_application_gadt_refinements specified_cv specified_atypes gadt_atype errors th_vars th_attrs
	= (errors bitor 2,th_vars,th_attrs)

set_gadt_type_refinements :: !AType !AType !*TypeHeaps -> (!Int,!*TypeHeaps)
set_gadt_type_refinements gadt_specified_type=:{at_attribute=specified_attribute,at_type=TA specified_tsi specified_atypes}
						  gadt_type=:          {at_attribute=gadt_attribute,     at_type=TA gadt_tsi gadt_atypes}
						  type_heaps=:{th_vars,th_attrs}
	| specified_tsi==gadt_tsi
		# th_attrs = set_gadt_attribute_var gadt_attribute specified_attribute th_attrs
		# th_attrs = set_gadt_attribute_vars gadt_atypes specified_atypes th_attrs
		#! (errors,th_vars,th_attrs) = set_atype_gadt_refinements gadt_specified_type gadt_type 0 th_vars th_attrs
		# type_heaps & th_vars = th_vars, th_attrs = th_attrs
		= (errors,type_heaps)
where
	set_gadt_attribute_var (TA_RootVar {av_info_ptr}) specified_attribute th_attrs
		= writePtr av_info_ptr (AVI_Attr specified_attribute) th_attrs
	set_gadt_attribute_var (TA_Var {av_info_ptr}) specified_attribute th_attrs
		= writePtr av_info_ptr (AVI_Attr specified_attribute) th_attrs

	set_gadt_attribute_var TA_Multi (TA_Var {av_info_ptr}) th_attrs
		= writePtr av_info_ptr (AVI_Attr TA_Multi) th_attrs
	set_gadt_attribute_var TA_Unique (TA_Var {av_info_ptr}) th_attrs
		= writePtr av_info_ptr (AVI_Attr TA_Unique) th_attrs

	set_gadt_attribute_var _ _ th_attrs
		= th_attrs

	set_gadt_attribute_vars [{at_attribute=gadt_attribute}:gadt_atypes] [{at_attribute=specified_attribute}:specified_atypes] th_attrs
		# th_attrs = set_gadt_attribute_var gadt_attribute specified_attribute th_attrs
		= set_gadt_attribute_vars gadt_atypes specified_atypes th_attrs
	set_gadt_attribute_vars _ _ th_attrs
		= th_attrs
set_gadt_type_refinements gadt_specified_type gadt_type type_heaps
	= (2,type_heaps)

remove_refined_type_vars :: ![TypeVar] !*TypeVarHeap -> (![TypeVar],!*TypeVarHeap)
remove_refined_type_vars [type_var=:{tv_info_ptr}:type_vars] th_vars
	= case sreadPtr tv_info_ptr th_vars of
		TVI_SubstSpecifiedType _
			-> remove_refined_type_vars type_vars th_vars
		_
			# (type_vars,th_vars) = remove_refined_type_vars type_vars th_vars
			-> ([type_var:type_vars],th_vars)
remove_refined_type_vars [] th_vars
	= ([],th_vars)

refine_type :: !Type !*TypeVarHeap !*AttrVarHeap -> (!Type,!*TypeVarHeap,!*AttrVarHeap)
refine_type type=:(TV tv=:{tv_info_ptr}) th_vars th_attrs
	# (tvi,th_vars) = readPtr tv_info_ptr th_vars
	= case tvi of
		TVI_SubstSpecifiedType refined_type
			-> refine_type refined_type th_vars th_attrs
		_
			-> (type,th_vars,th_attrs)
refine_type (TA tsi atypes) th_vars th_attrs
	# (atypes,th_vars,th_attrs) = refine_atypes atypes th_vars th_attrs
	= (TA tsi atypes,th_vars,th_attrs)
refine_type (TAS tsi atypes sl) th_vars th_attrs
	# (atypes,th_vars,th_attrs) = refine_atypes atypes th_vars th_attrs
	= (TAS tsi atypes sl,th_vars,th_attrs)
refine_type (atype1 --> atype2) th_vars th_attrs
	# (atype1,th_vars,th_attrs) = refine_atype atype1 th_vars th_attrs
	  (atype2,th_vars,th_attrs) = refine_atype atype2 th_vars th_attrs
	= (atype1 --> atype2,th_vars,th_attrs)
refine_type (CV cv=:{tv_info_ptr} :@: atypes) th_vars th_attrs
	# (atypes,th_vars,th_attrs) = refine_atypes atypes th_vars th_attrs
	# (tvi,th_vars) = readPtr tv_info_ptr th_vars
	= case tvi of
		TVI_SubstSpecifiedType refined_type
			# (refined_type,th_vars,th_attrs) = refine_type refined_type th_vars th_attrs
			-> (simplifyTypeApplication refined_type atypes,th_vars,th_attrs)
		with
			simplifyTypeApplication (TV tv) type_args
				= CV tv :@: type_args
			simplifyTypeApplication (TA type_cons=:{type_arity} cons_args) type_args
				= TA {type_cons & type_arity = type_arity + length type_args} (cons_args ++ type_args)
			simplifyTypeApplication (TAS type_cons=:{type_arity} cons_args strictness) type_args
				= TAS {type_cons & type_arity = type_arity + length type_args} (cons_args ++ type_args) strictness
			simplifyTypeApplication (cons_var :@: types) type_args
				= cons_var :@: (types ++ type_args)
			simplifyTypeApplication TArrow [type1, type2]
				= type1 --> type2
			simplifyTypeApplication TArrow [type]
				= TArrow1 type
			simplifyTypeApplication (TArrow1 type1) [type2]
				= type1 --> type2
		_
			-> (CV cv :@: atypes,th_vars,th_attrs)
refine_type (TArrow1 atype) th_vars th_attrs
	# (atype,th_vars,th_attrs) = refine_atype atype th_vars th_attrs
	= (TArrow1 atype,th_vars,th_attrs)
refine_type (TFA atypevars type) th_vars th_attrs
	# (type,th_vars,th_attrs) = refine_type type th_vars th_attrs
	= (TFA atypevars type,th_vars,th_attrs)
refine_type (TFAC atypevars type type_contexts) th_vars th_attrs
	# (type,th_vars,th_attrs) = refine_type type th_vars th_attrs
	  (type_contexts,th_vars,th_attrs) = refine_type_contexts type_contexts th_vars th_attrs
	= (TFAC atypevars type type_contexts,th_vars,th_attrs)
refine_type type th_vars th_attrs
	= (type,th_vars,th_attrs)

refine_atype :: !AType !*TypeVarHeap !*AttrVarHeap -> (!AType,!*TypeVarHeap,!*AttrVarHeap)
refine_atype atype=:{at_attribute=TA_Var {av_info_ptr},at_type} th_vars th_attrs
	# (av_info, th_attrs) = readPtr av_info_ptr th_attrs
	= case av_info of
		AVI_Attr TA_Multi
			# (at_type,th_vars,th_attrs) = refine_type at_type th_vars th_attrs
			= ({atype & at_attribute=TA_Multi,at_type=at_type},th_vars,th_attrs)
		AVI_Attr TA_Unique
			# (at_type,th_vars,th_attrs) = refine_type at_type th_vars th_attrs
			= ({atype & at_attribute=TA_Unique,at_type=at_type},th_vars,th_attrs)
		_
			# (at_type,th_vars,th_attrs) = refine_type at_type th_vars th_attrs
			= ({atype & at_type=at_type},th_vars,th_attrs)
refine_atype atype=:{at_attribute=TA_RootVar {av_info_ptr},at_type} th_vars th_attrs
	# (av_info, th_attrs) = readPtr av_info_ptr th_attrs
	= case av_info of
		AVI_Attr TA_Multi
			# (at_type,th_vars,th_attrs) = refine_type at_type th_vars th_attrs
			= ({atype & at_attribute=TA_Multi,at_type=at_type},th_vars,th_attrs)
		AVI_Attr TA_Unique
			# (at_type,th_vars,th_attrs) = refine_type at_type th_vars th_attrs
			= ({atype & at_attribute=TA_Unique,at_type=at_type},th_vars,th_attrs)
		_
			# (at_type,th_vars,th_attrs) = refine_type at_type th_vars th_attrs
			= ({atype & at_type=at_type},th_vars,th_attrs)
refine_atype atype=:{at_type} th_vars th_attrs
	# (at_type,th_vars,th_attrs) = refine_type at_type th_vars th_attrs
	= ({atype & at_type=at_type},th_vars,th_attrs)

refine_atypes :: ![AType] !*TypeVarHeap !*AttrVarHeap -> (![AType],!*TypeVarHeap,!*AttrVarHeap)
refine_atypes [atype:atypes] th_vars th_attrs
	# (atypes,th_vars,th_attrs) = refine_atypes atypes th_vars th_attrs
	  (atype,th_vars,th_attrs) = refine_atype atype th_vars th_attrs
	= ([atype:atypes],th_vars,th_attrs)
refine_atypes [] th_vars th_attrs
	= ([],th_vars,th_attrs)

refine_types :: ![Type] !*TypeVarHeap !*AttrVarHeap -> (![Type],!*TypeVarHeap,!*AttrVarHeap)
refine_types [type:types] th_vars th_attrs
	# (types,th_vars,th_attrs) = refine_types types th_vars th_attrs
	  (type,th_vars,th_attrs) = refine_type type th_vars th_attrs
	= ([type:types],th_vars,th_attrs)
refine_types [] th_vars th_attrs
	= ([],th_vars,th_attrs)

refine_type_contexts :: ![TypeContext] !*TypeVarHeap !*AttrVarHeap -> (![TypeContext],!*TypeVarHeap,!*AttrVarHeap)
refine_type_contexts [type_context=:{tc_types}:type_contexts] th_vars th_attrs
	# (tc_types,th_vars,th_attrs) = refine_types tc_types th_vars th_attrs
	  (type_contexts,th_vars,th_attrs) = refine_type_contexts type_contexts th_vars th_attrs
	= ([{type_context & tc_types=tc_types}:type_contexts],th_vars,th_attrs)
refine_type_contexts [] th_vars th_attrs
	= ([],th_vars,th_attrs)

add_fresh_gadt_type_vars :: ![TypeVar] ![TypeVar] !*TypeVarHeap -> (![TypeVar],!*TypeVarHeap)
add_fresh_gadt_type_vars [{tv_info_ptr}:gadt_type_vars] fresh_gadt_type_vars th_vars
	# (tvi,th_vars) = readPtr tv_info_ptr th_vars
	= case tvi of
		TVI_Empty
			-> add_fresh_gadt_type_vars gadt_type_vars fresh_gadt_type_vars th_vars
		TVI_SubstSpecifiedForGADType (TV tv)
			-> add_fresh_gadt_type_vars gadt_type_vars [tv:fresh_gadt_type_vars] th_vars
		TVI_SubstSpecifiedForGADType _
			-> add_fresh_gadt_type_vars gadt_type_vars fresh_gadt_type_vars th_vars
add_fresh_gadt_type_vars [] fresh_gadt_type_vars th_vars
	= (fresh_gadt_type_vars,th_vars)

add_fresh_gadts_type_vars :: ![[TypeVar]] ![TypeVar] !*TypeVarHeap -> (![TypeVar],!*TypeVarHeap)
add_fresh_gadts_type_vars [more_gadt_type_vars:more_gadts_type_vars] fresh_gadt_type_vars th_vars
	# (fresh_gadt_type_vars,th_vars) = add_fresh_gadt_type_vars more_gadt_type_vars fresh_gadt_type_vars th_vars
	= add_fresh_gadts_type_vars more_gadts_type_vars fresh_gadt_type_vars th_vars
add_fresh_gadts_type_vars [] fresh_gadt_type_vars th_vars
	= (fresh_gadt_type_vars,th_vars)

refine_function_type :: !SymbolType ![TypeVar] ![[TypeVar]] !*TypeVarHeap !*AttrVarHeap -> (!SymbolType,!*TypeVarHeap,!*AttrVarHeap)
refine_function_type ft=:{st_vars,st_args,st_result,st_context} gadt_type_vars more_gadt_type_vars th_vars th_attrs
	# (st_vars,th_vars) = remove_refined_type_vars st_vars th_vars
	  (st_args,th_vars,th_attrs) = refine_atypes st_args th_vars th_attrs
	  (st_result,th_vars,th_attrs) = refine_atype st_result th_vars th_attrs
	  (st_context,th_vars,th_attrs) = refine_type_contexts st_context th_vars th_attrs
	  (fresh_gadt_type_vars,th_vars) = add_fresh_gadt_type_vars gadt_type_vars [] th_vars
	  (fresh_gadt_type_vars,th_vars) = add_fresh_gadts_type_vars more_gadt_type_vars fresh_gadt_type_vars th_vars
	  st_vars = if (fresh_gadt_type_vars=:[]) st_vars (st_vars++fresh_gadt_type_vars)
	  ft & st_vars=st_vars, st_args=st_args, st_result=st_result, st_context=st_context
	= (ft,th_vars,th_attrs)
